# -*- coding: utf-8 -*-

"""
Created on Tue Sep 18 22:38:47 2018

@author: vahh
"""
from IPython import get_ipython
get_ipython().magic('reset -sf')

import time, sys, yaml, os
import bisect
from matplotlib.font_manager import FontProperties

if os.path.abspath('libraries/import_data') not in sys.path:
    sys.path.append('libraries/import_data')
    sys.path.append('libraries/debug_figure')
    sys.path.append('libraries/cam_utils')
    sys.path.append('libraries/ground_truth')
    sys.path.append('libraries/datalog_handler')
    sys.path.append('libraries/perception_utils')
    sys.path.append('libraries/ros_utils')
    sys.path.append('libraries/ros_utils/dist_packages')
    sys.path.append('libraries/tictoc')

from import_data_class import *
import file_handling_utils
from debug_figure_axes import *
from cam_utils import *
import ground_truth
import handle_logs


def main(paths):
    gt_path = paths['gt_path']    
    hl = handle_logs.HandleLogs('paths.yaml', math.pi/2, exp_original_only=True)
    gt, is_gt_available = ground_truth.getGTlidarBinaryClassification(gt_path, hl.exp_original, hl.lidar.ts)    
    image_path = os.path.join(gt_path, str(gt.timestamp[0])+'.png')
    usable_path = os.path.join(gt_path, 'Usable')
    not_usable_path = os.path.join(gt_path, 'NotUsable')

    if not os.path.exists(gt_path): 
        return 'gt path does not exist'
    if not is_gt_available:
        return 'gt is not available'
    if not os.path.isfile(image_path):
        return 'lidar images not created'
    if not os.path.exists(usable_path):
        os.makedirs(usable_path)
    if not os.path.exists(not_usable_path):
        os.makedirs(not_usable_path)
    for i in range(0, len(gt.timestamp)):
        image_path = os.path.join(gt_path, str(gt.timestamp[i])+'.png')
        chosen_dir = not_usable_path if gt.is_scan_valid[i] == 0 else usable_path
        new_image_path = os.path.join(chosen_dir, str(gt.timestamp[i])+'.png')
        os.rename(image_path, new_image_path)
        
    return 'LiDAR images were organized'
if __name__ == '__main__':    
    with open('paths.yaml') as file:
        paths = yaml.load(file)
        print(main(paths))
