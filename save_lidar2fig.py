# -*- coding: utf-8 -*-

"""
Created on Tue Sep 18 22:38:47 2018

@author: vahh
"""
from IPython import get_ipython
get_ipython().magic('reset -sf')

import time, sys, yaml, os
import bisect
from matplotlib.font_manager import FontProperties

if os.path.abspath('libraries/import_data') not in sys.path:
    sys.path.append('libraries/import_data')
    sys.path.append('libraries/debug_figure')
    sys.path.append('libraries/cam_utils')
    sys.path.append('libraries/ground_truth')
    sys.path.append('libraries/datalog_handler')
    sys.path.append('libraries/perception_utils')
    sys.path.append('libraries/ros_utils')
    sys.path.append('libraries/ros_utils/dist_packages')
    sys.path.append('libraries/tictoc')

from import_data_class import *
import file_handling_utils
from debug_figure_axes import *
from cam_utils import *
import ground_truth
import handle_logs

# import path from paths.yaml
with open('paths.yaml') as file:
    paths = yaml.load(file)

hl = handle_logs.HandleLogs('paths.yaml', math.pi/2, exp_original_only=True)
gt_path = paths['gt_path']
gt = ground_truth.getGTlidarBinaryClassification(gt_path, hl.exp_original, hl.lidar.ts)

x_i = -1.5
x_s = 1.5
y_i = -0.5
y_s = 3.5  

rot_angle = math.pi/2
lidar = hl.lidar.df
angle = np.linspace(-3*math.pi/4+rot_angle, 3*math.pi/4+rot_angle, 1081, endpoint = False)
l_ts = np.array(list(lidar))

plt.close('all')
fig = plt.figure(1, figsize=[4.8, 6.4])
rect = [0.0, 0.0, 1.0, 1.0]
ax = fig.add_axes(rect)
x=[]
y=[]
pl, = ax.plot(x, y, 'k.', ms=1)

for i in range(0, len(lidar)):
# for i in range(0, 1):
    ts_lidar = l_ts[i]
    l = lidar[ts_lidar]
    x = []
    y = []
    for j in range(0, len(l)):
        x.append(0.001*l[j]*math.cos(angle[j]))
        y.append(0.001*l[j]*math.sin(angle[j]))
    pl.set_xdata(x)
    pl.set_ydata(y)
    ax.set_xlim([x_i, x_s])
    ax.set_ylim([y_i, y_s])     
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.set_aspect('equal')        
    ax.set_axis_off()        
    fig_name = '%d.png' % l_ts[i]
    fig_path = os.path.join(gt_path, fig_name)
    print('saving ' + str(i) + '/' + str(len(lidar)) + str(fig_path))
    fig.savefig(fig_path, pad_inches=0,  dpi=50)
