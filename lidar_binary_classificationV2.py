# -*- coding: utf-8 -*-

"""
Created on Tue Sep 18 22:38:47 2018

@author: vahh
"""
from IPython import get_ipython
get_ipython().magic('reset -sf')

import time, sys, yaml, os
import bisect
from matplotlib.font_manager import FontProperties

if os.path.abspath('libraries/import_data') not in sys.path:
    sys.path.append('libraries/import_data')
    sys.path.append('libraries/debug_figure')
    sys.path.append('libraries/cam_utils')
    sys.path.append('libraries/ground_truth')
    sys.path.append('libraries/datalog_handler')
    sys.path.append('libraries/perception_utils')
    sys.path.append('libraries/ros_utils')
    sys.path.append('libraries/ros_utils/dist_packages')
    sys.path.append('libraries/tictoc')

from import_data_class import *
import file_handling_utils
from debug_figure_axes import *
from cam_utils import *
import ground_truth
import handle_logs

# import path from paths.yaml
with open('paths.yaml') as file:
    paths = yaml.load(file)

hl = handle_logs.HandleLogs('paths.yaml', math.pi/2, exp_original_only=True, ts_label_to_sync='lidar_ts_ms')
gt_path = paths['gt_path']
gt, is_gt_available = ground_truth.getGTlidarBinaryClassification(gt_path, hl.exp_original, hl.lidar.ts)

final_path = paths['path2organizeLidarImages']
usable_path = os.path.join(final_path, 'usable')
not_usable_path = os.path.join(final_path, 'notUsable')
if not os.path.exists(final_path):
    try:
        os.makedirs(final_path)
    except:
        print('\n\nInvalid path to organize lidar images. Provided path was ' + str(final_path))
        sys.exit()
    input('Folder to organize lidar images created at ' + str(final_path)+'\n\n\t\tPress Enter to continue...\n')

if not os.path.exists(usable_path):
    os.makedirs(usable_path)
    input('Folder to separate usable lidar images created at ' + str(usable_path)+'\n\n\t\tPress Enter to continue...\n')
if not os.path.exists(not_usable_path):
    os.makedirs(not_usable_path)
    input('Folder to separate not usable lidar images created at ' + str(not_usable_path)+'\n\n\t\tPress Enter to continue...\n')
                    
input('Ground truth file with a number to represent each scan (1 for usable, 0 for not usable, -2 for unchecked) was created at\n\n\t' + str(gt_path) + "\n\nIf not the right place, please edit gt_path in paths.yaml. Press Enter to continue\n\n")

x_i = -3
x_s = 3
y_i = -0.5
y_s = 5.5  

x_i = -1.5
x_s = 1.5
y_i = -0.5
y_s = 3.5  

rot_angle = math.pi/2
# timestamp for LiDAR measurements
p_original = hl.p_original.df
lidar = hl.lidar.df
angle = np.linspace(-3*math.pi/4+rot_angle, 3*math.pi/4+rot_angle, 1081, endpoint = False)
ts_original = np.array(p_original.timestamp)
#ts_original = p_original.lidar_tags_ts_ms
l_ts = np.array(list(lidar))
n_scans = 1
gt_save_time = 0

new_pts_to_save = 0
auto_save_s = 5  

index_first_zero = 0
cur_t = index_first_zero
last_saved_t = index_first_zero
l_idx = np.arange(len(l_ts))

def plots(t):
    global cur_t
    print('#########################################')
    f.fig.patch.set_facecolor('none') 
        
    if t < 0: 
        t = 0
    elif t >= len(l_ts)-1:
        t = len(l_ts) - 1 
    cur_t = t 

    hl.sync(cur_t)
    k_original = hl.p_original.idx
    if k_original > 0:
        kk = k_original-1
    else:
        kk=0

    print('plots t: ', t, ' k_original', k_original, ' l_ts[t]: ', l_ts[t], 'gt[cur_t]:', gt.timestamp[cur_t] ) 
    ##
    #   UPDATE 1ST FIGURE
    ##   
    f.lidar.update(cur_t, \
        title_str= paths['exp_original_path'] + \
            '\nCURRENT index: ' + str(cur_t) + '/'+str(len(l_ts)-1)+  \
            ' l_ts ' + str(l_ts[cur_t]) + \
            ' pl_ts ' + str(ts_original[k_original]) + \
                 '\nUSE THIS PLOT')
    pl_gt_is_scan_valid.update(k=cur_t, x=l_idx, y=gt.is_scan_valid, title_str='is_scan_valid: ' + f3(gt.is_scan_valid[cur_t]))
    pl_automatic.update(k_original, title_str='autonomous mode ' + str(p_original.Automatic[k_original]) )
    cam.update(t)   

    pl_line_l.update(k=k_original, rotate_rad=-p_original.heading[kk])
    pl_line_r.update(k=k_original, rotate_rad=-p_original.heading[kk])

    num_usable = len(os.listdir(usable_path))
    num_not_usable = len(os.listdir(not_usable_path))
    num_total = num_usable + num_not_usable
    num_total = num_total if num_total > 0 else 1
    bars[0].set_height(num_usable)
    bars[1].set_height(num_not_usable)
    pl_num_usable.ax.set_ylim([0,np.max([num_usable, num_not_usable])])
    pl_num_usable.ax.set_title('num_usable ' + str(num_usable) + '(' + f3(100*num_usable/num_total) + '%) num_not_usable ' + str(num_not_usable) + '(' + f3(100*num_not_usable/num_total) + '%) ' )

    if time.mktime(time.localtime()) - gt_save_time > auto_save_s and new_pts_to_save > 10:
        print('AUTO SAVE')
        save2csv()  

def createLidarImageAndSave2path(path, colllection_name, ts, l):
    y_i = -1.5
    y_s = 1.5
    x_i = -0.5
    x_s = 3.5
    plt.ioff()
    fig = plt.figure('temp', figsize=[3.2, 2.4])
    rect = [0.0, 0.0, 1.0, 1.0]
    ax = fig.add_axes(rect)
    x=[]
    y=[]
    angle = np.linspace(-3*math.pi/4, 3*math.pi/4, 1081, endpoint = False)
    for j in range(0, len(l)):
        x.append(0.001*l[j]*math.cos(angle[j]))
        y.append(0.001*l[j]*math.sin(angle[j]))
    pl, = ax.plot(x, y, 'k.', ms=1)
    ax.set_xlim([x_i, x_s])
    ax.set_ylim([y_i, y_s])     
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)     
    ax.set_axis_off()   
    ax.set_aspect('equal')    
    fig_name = colllection_name + '_%d.png' % ts
    fig_path = os.path.join(path, fig_name)
    print('saving to ' + str(fig_path))
    fig.savefig(fig_path, pad_inches=0,  dpi=100)
    plt.close()    
    plt.ion()


def press(event):
    global gt
    global cur_t
    global gt_save_time
    global new_pts_to_save, n_scans
    sys.stdout.flush()
    
    if event.key == 's':
        save2csv()
    elif event.key == 'right':
        cur_t += n_scans
        f.slider_scan.set_val(cur_t)        
    elif event.key == 'left':
        cur_t-= n_scans
        f.slider_scan.set_val(cur_t)        
    elif event.key == 'up':
        n_scans += 1
        if n_scans > 50:
            n_scans = 50   
        f.n_scans_text.set_text('step for <previous> <next> scan: ' + str(n_scans))        
    elif event.key == 'down':        
        n_scans -= 1
        if n_scans < 1:
            n_scans = 1 
        f.n_scans_text.set_text('step for <previous> <next> scan: ' + str(n_scans))          
    elif event.key == '1': #only left side is valid       
        gt.loc[cur_t] = [l_ts[cur_t], 1]          
        createLidarImageAndSave2path(usable_path, hl.exp_original, l_ts[cur_t], hl.lidar.df[l_ts[cur_t]]) 
        cur_t += n_scans           
        f.slider_scan.set_val(cur_t)          
    elif event.key == '2':     
        gt.loc[cur_t] = [l_ts[cur_t], 0]        
        createLidarImageAndSave2path(not_usable_path, hl.exp_original, l_ts[cur_t], hl.lidar.df[l_ts[cur_t]])           
        cur_t += n_scans           
        f.slider_scan.set_val(cur_t)    
    elif event.key == '3':
        gt.loc[cur_t] = [l_ts[cur_t], -1]           
        cur_t += n_scans           
        f.slider_scan.set_val(cur_t)  
               
    f.fig.canvas.draw()     

def save2csv():
    global gt_save_time
    global new_pts_to_save
    global last_saved_t
    last_saved_t = cur_t
    gt_save_time = time.mktime(time.localtime())    
    name = os.path.join(gt_path, 'gt_lidar_binary_classification_' + hl.exp_original +  time.strftime("_%Y%m%d_%H%M%S", time.localtime()) + '.csv')
    f.fig.patch.set_facecolor('xkcd:pastel green')
    f.fig.canvas.draw()
    print ('GT saved to', name)
    gt.to_csv(name)
    new_pts_to_save = 0 
    
plt.close("all")
f = DebugFigure(plots_fcn=plots)
f.initFigure(figsize=[15,8],\
              custom_keys=True,\
              custom_press = press,
              subplots_hspace=1.2, subplots_wspace=0.9,\
              window_width=1200, window_height=900, \
                  window_left=50, window_top=50)  
                  
# Initialize buttons and sliders for interval of 0 to timestamp length
f.initWidgets(len(l_ts)-1, width=0.5) 

print('starting automatic')
pl_automatic = DebugFigure.XY_point(fig=f.fig, rect=[0.05, 0.16, 0.4, 0.03], x=p_original.timestamp, y=p_original.Automatic, marker='.', marker_color='b', marker_size=1, _xticks=False, title_str='autonomous mode' )
#%%plot to choose gt
f.lidar = DebugFigure.LiDAR(fig=f.fig, rect=[0.4, 0.2, 0.7, 0.7], \
                    xaxis_min=x_i, xaxis_max=x_s, yaxis_min=y_i, yaxis_max=y_s,\
                    angle=angle, lidar=lidar, timestamp=l_ts, xlabel_on=False, \
                    ylabel_on=False)
f.lidar.ax.set_xlim([x_i, x_s])
f.lidar.ax.set_ylim([y_i, y_s]) 
f.fig.text(0.7, 0.01, 'USE KEYBOARD\n' \
                        '[<] prev scan              [>] next scan\n'\
                        '[^] increase scan step     [v] decrease scan step\n'\
                        '[1] report valid scan      [2] report invalid scan\n'\
                        '[3] report scan from outside rows\n'\
                        '[s] save to file' )

pl_gt_is_scan_valid = DebugFigure.XY_full(fig=f.fig, x=l_idx, y=gt.is_scan_valid, marker_color='k', 
                                   marker='o', marker_size=2, update_marker='x', update_color='r', 
                                   rect=[0.05, 0.1, 0.4, 0.03], _xticks=False)
pl_gt_is_scan_valid.ax.set_ylim([-3, 2])
  
#pl_line_l = DebugFigure.XY_line(x0=p_original.line_x0_l, y0=p_original.line_y0_orig_l, x1=p_original.line_x1_l, y1=p_original.line_y1_orig_l, rotate_rad=rot_angle, marker='x', marker_color='green', marker_size=10, linewidth=0, _ax=f.lidar.ax)
#pl_line_r = DebugFigure.XY_line(x0=p_original.line_x0_r, y0=p_original.line_y0_orig_r, x1=p_original.line_x1_r, y1=p_original.line_y1_orig_r, rotate_rad=rot_angle, marker='x', marker_color='green', marker_size=10, linewidth=0, _ax=f.lidar.ax)
  
pl_line_l = DebugFigure.XY_line(x0=p_original.line_x0_l, y0=p_original.line_y0_l, x1=p_original.line_x1_l, y1=p_original.line_y1_orig_l, rotate_rad=rot_angle, marker='x', marker_color='red', marker_size=5, linecolor='red', linewidth=0.5, _ax=f.lidar.ax)
pl_line_r = DebugFigure.XY_line(x0=p_original.line_x0_r, y0=p_original.line_y0_r, x1=p_original.line_x1_r, y1=p_original.line_y1_orig_r, rotate_rad=rot_angle, marker='x', marker_color='red', marker_size=5, linecolor='red', linewidth=0.5, _ax=f.lidar.ax)

num_usable = len(os.listdir(usable_path))
num_not_usable = len(os.listdir(not_usable_path))
pl_num_usable = DebugFigure.XY_point(fig=f.fig, rect=[0.03, 0.25, 0.41, 0.1],marker_size=0, linewidth=0, update_marker_size=0)
bars = pl_num_usable.ax.bar(['usable', 'notUsable'], [num_usable, num_not_usable], color=['green', 'salmon'])
pl_num_usable.ax.set_xlim([-1,2])
#cam = DebugFigure.Camera(fig=f.fig, rect=[0.01, 0.15, 0.25, 0.25],\
cam = DebugFigure.Camera(fig=f.fig, rect=[0.1, 0.7, 0.20, 0.20],\
                                    alias='front',idx=0,ts_cam=hl.front_ts_cam)

# 
#%% SLIDER
idx= 0
f.slider_scan.set_val(idx)
plots(idx)