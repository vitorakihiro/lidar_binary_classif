import cv2
import numpy as np
import bisect 

DEBUG = False

class TScam:
    __slots__ = ['prev_ref_index_num', 'ref_index_num', 'frame_num', 'blank_image', 'timestamp', 'ref_ts', 'cap', 'fps']
    def __init__(self, logs=None, exp='', location="front"):
        self.prev_ref_index_num = 0
        self.ref_index_num = 0
        self.frame_num = 0
        self.blank_image= np.zeros([100,100,3],dtype=np.uint8)
        
        try:
            if location is "front":
                video_filepath = logs.cam_front_fp[exp]
                self.timestamp = logs.cam_front[exp].capture_time_ms
            elif location is "left":
                video_filepath = logs.cam_left_fp[exp]
                self.timestamp = logs.cam_left[exp].capture_time_ms
            elif location is "right":
                video_filepath = logs.cam_right_fp[exp]
                self.timestamp = logs.cam_right[exp].capture_time_ms
            elif location is "back":
                video_filepath = logs.cam_back_fp[exp]
                self.timestamp = logs.cam_back[exp].capture_time_ms
            
            self.ref_ts = logs.pTS[exp].timestamp
            self.cap = cv2.VideoCapture(str(video_filepath))
            self.fps = self.cap.get(cv2.CAP_PROP_FPS) 
            print('TScam created. There are ' + str(self.timestamp.size) + ' frames')
        except:
            self.timestamp = []
            self.ref_ts = []
            self.cap = None
            self.fps = None
            print('Check cam filepath')

    # Camera related
    def getFrame(self, ref_index_num=0, f_rotate=False):
        diff = 0
        print('ref_ts len ' + str(len(self.ref_ts)) + ' self.timestamp len ' + str(len(self.timestamp)))
        if len(self.ref_ts) > 0:
            if DEBUG:
                print('prev_frame_num: ' + str(self.frame_num)) 
            self.frame_num = bisect.bisect_left(self.timestamp, self.ref_ts[ref_index_num], hi=len(self.timestamp)-1)

            required_ts = self.ref_ts[ref_index_num]
            '''
            b_continue = True
            i = ref_index_num
            while b_continue and i > 0:
                if  self.timestamp[i] < self.ref_ts[ref_index_num]:
                    b_continue = False
                i-=1
                
            b_continue = True
            while i < len(self.timestamp) - 1 and b_continue:
                if required_ts < self.timestamp[i]:
                    self.frame_num = i
                    b_continue = False
                elif required_ts >= self.timestamp[i] and required_ts < self.timestamp[i+1]:
                    self.frame_num = i
                    b_continue = False
                elif i == len(self.timestamp) - 2:
                    self.frame_num = len(self.timestamp) -2 
                    b_continue = False
                i+=1
                '''
            diff = self.timestamp[self.frame_num] - required_ts
            print('getFrame lts[' + str(ref_index_num) + ']: ' + str(required_ts) + ' cam.timestamp[' +str(self.frame_num) + ']: ' + str(self.timestamp[self.frame_num]) + ' diff: ' + str(diff) + 'ms')
            self.cap.set(cv2.CAP_PROP_POS_FRAMES,self.frame_num)
            if DEBUG:
                print('cap set to: ' + str(self.cap.get(cv2.CAP_PROP_POS_FRAMES)))
        
            #Read the next frame from the video
            ret, frame = self.cap.read()
            if DEBUG:
                print('cap.read() ret:' + str(ret)  )
            if ret:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                if f_rotate:    
                    rows,cols,ch = frame.shape    
                    M = cv2.getRotationMatrix2D((cols/2,rows/2),180,1)
                    dst = cv2.warpAffine(frame,M,(cols,rows))
                    return dst, diff
                else:
                    return frame, diff
            else:
                return self.blank_image, diff
        else:
            return self.blank_image, diff