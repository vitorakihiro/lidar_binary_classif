import pandas as pd
import sys, os
from import_data_class import *
from file_handling_utils import *
from perception_utils import *

def f3(val):
    return format(val,'3.3f')
def rotateXY(ox, oy, rotate_rad):
    if ox!= 0 and oy!= 0 and not math.isnan(ox) and not math.isnan(oy):
        nx = math.cos(rotate_rad)*ox + math.sin(rotate_rad)*oy
        ny = -math.sin(rotate_rad)*ox + math.cos(rotate_rad)*oy
        #print('ox ' + f3(ox) + ' oy ' + f3(oy) + ' nx ' + f3(nx) + ' ny ' + f3(ny))
    else:
        nx = ox
        ny = oy
    return nx, ny

def getGT(gt_path, exp_original_path, ts_ref, rotate_rad=0):
    exp_original_path = 'gt_' + exp_original_path
    gt_file = getLatestMatchingFile(gt_path, exp_original_path)
    cols = ['ts_ms', 'xl0', 'yl0', 'xl1', 'yl1', 'xr0', 'yr0', 'xr1', 'yr1']
    if os.path.isfile(gt_file):
        print('GT AVAILABLE')
        gt = pd.read_csv(gt_file, usecols = cols)
    else:      
        data = [[ts_ref[0]] + [0]*(len(cols)-1)]
        for i in range(1,len(ts_ref)):
            data.append([ts_ref[i]] + [0]*(len(cols)-1))
        gt = pd.DataFrame(columns=cols, data=data)
        
    gt_dl = []
    gt_dr = []

    if rotate_rad is not 0:
        for i in range(len(gt.xl0)):
            nx,ny = rotateXY(gt.xl0[i].copy(), gt.yl0[i].copy(), rotate_rad) 
            gt.xl0.at[i] = nx
            gt.yl0.at[i] = ny
            nx,ny = rotateXY(gt.xl1[i].copy(), gt.yl1[i].copy(), rotate_rad)
            gt.xl1.at[i] = nx
            gt.yl1.at[i] = ny
            nx,ny = rotateXY(gt.xr0[i].copy(), gt.yr0[i].copy(), rotate_rad)
            gt.xr0.at[i] = nx
            gt.yr0.at[i] = ny
            nx,ny = rotateXY(gt.xr1[i].copy(), gt.yr1[i].copy(), rotate_rad)
            gt.xr1.at[i] = nx
            gt.yr1.at[i] = ny
    for i in range(0, len(gt.xl0)):
        #if gt.xl0[i] != 0 or gt.xr0[i] != 0 and i > index_first_zero :
        #    index_first_zero = i        
        x = [gt.xl0[i], gt.xl1[i]]
        y = [gt.yl0[i], gt.yl1[i]]
        aux,_,_,_,_ = fcnGetLateralDistance_y_ax_b(x, y)
        if gt.xl0[i] != 0.0:
            aux,_,_,_,_ = fcnGetLateralDistance_y_ax_b(x, y, debug=True)
            print(str(i) + ' x ' + f3(gt.xl0[i]) + ' ' + f3(gt.xl1[i]) + ' y ' + f3(gt.yl0[i]) + ' ' + f3(gt.yl1[i]) + ' aux ' + f3(aux))
        gt_dl.append(aux)
        x = [gt.xr0[i], gt.xr1[i]]
        y = [gt.yr0[i], gt.yr1[i]]
        aux,_,_,_,_ = fcnGetLateralDistance_y_ax_b(x, y)
        gt_dr.append(aux)
    
    t_gt = np.array(gt.ts_ms)
    gt_dl = np.array(gt_dl)
    gt_dr = np.array(gt_dr)    
    gt_lw = gt_dl + gt_dr
    gt_diff = 0.5*(gt_dr-gt_dl)
    is_gt_available = True

    return gt, gt_dl, gt_dr, t_gt, gt_lw, gt_diff, is_gt_available

def getGTlidarBinaryClassification(gt_path, exp_original_path, l_ts):
    exp_original_path = 'gt_lidar_binary_classification_' + exp_original_path
    gt_file = getLatestMatchingFile(gt_path, exp_original_path)
    cols = ['timestamp', 'is_scan_valid']
    print(gt_file)
    if os.path.isfile(gt_file):
        print('GT AVAILABLE')
        is_gt_available = True
        gt = pd.read_csv(gt_file, usecols=cols)
    else:      
        data = [[l_ts[0], -2]]
        is_gt_available = False
        for i in range(1,len(l_ts)):
            data.append([l_ts[i], -2])
        gt = pd.DataFrame(columns=cols,data=data)
      
    return gt, is_gt_available

def getGTlidarTags(gt_path, exp_original_path, l_ts, n_tags_per_scan):
    exp_original_path = 'gt_lidar_tags_' + exp_original_path
    gt_file = getLatestMatchingFile(gt_path, exp_original_path)
    cols = ['ts_ms']
    for i in range(0, n_tags_per_scan):
        cols.append(str(i))
    print(gt_file)
    if os.path.isfile(gt_file):
        print('GT AVAILABLE')
        gt_lidar_tags = pd.read_csv(gt_file, usecols=cols)
    else:      
        data = [[l_ts[0]] + [0]*n_tags_per_scan]
        for i in range(1,len(l_ts)):
            data.append([l_ts[i]] + [0]*n_tags_per_scan)
        gt_lidar_tags = pd.DataFrame(columns=cols,data=data)
      
    return gt_lidar_tags

