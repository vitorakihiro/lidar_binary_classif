import math

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False
    
def f3(val):
    try:
        v = float(val)
        if v.is_integer():
            return str(int(v))
        if not isinstance(v, float):
            v = 0.0
        return str(format(v,'3.3f'))
    except:
        return str(val)
    
def rotate(x, y, angle):
    #R = [cos(th) -sin(th); sin(th) cos(th)]
    x_rot=[]
    y_rot=[]
    for i in range(0, len(x)):
        x_rot.append(x[i]*math.cos(angle) - y[i]*math.sin(angle))
        y_rot.append(x[i]*math.sin(angle) + y[i]*math.cos(angle))
    x_rot=np.array(x_rot)
    y_rot=np.array(y_rot)
    return x_rot, y_rot
	
def zeus_dlatlon2dxy(lat1, lon1, lat2, lon2):
	R = 6367300

	rlat1 = lat1*math.pi/180
	rlat2 = lat2*math.pi/180
	rlon1 = lon1*math.pi/180
	rlon2 = lon2*math.pi/180
	dlat = rlat2 - rlat1
	dlon = rlon2 - rlon1

	dx = R*dlon*math.cos(0.5*(rlat1+rlat2))
	dy = R*dlat

	return dx, dy
