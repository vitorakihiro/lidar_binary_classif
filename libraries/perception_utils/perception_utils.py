#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 15:27:56 2019

@author: hiro
"""
import math
import numpy as np

def f3(val):
    return format(val,'3.3f')
def fcnGetLateralDistance(x, y, L=0, debug=False):
    R, slope, intercept = fcnCoefficientsLeastSquares(x, y)
    '''
    if slope != 0:
        d = abs(intercept/slope)*abs(math.sin(math.atan(slope)))
    else:
        d = abs(intercept)
    '''
    d = abs(intercept)      
    dc = (intercept/slope+L)*slope
    return d, dc, R, slope, intercept


def fcnGetLateralDistance_y_ax_b(x, y, L=0, debug=False):
    R, slope, intercept = fcnCoefficientsLeastSquares(x, y)
    d = fcnOrthogonalDistanceToLine(0, 0, slope, intercept)    
    dc = -1
    return d, dc, R, slope, intercept


def fcnOrthogonalDistanceToLine(x0, y0, m, k):
    d = abs(k + m*x0 -y0)/math.sqrt(m*m+1)
    return d

def fcnCoefficientsLeastSquares( x, y ):

    x = np.array(x)
    y = np.array(y)
    x = x[~np.isnan(x)]
    y = y[~np.isnan(y)]
    Sxy = 0
    Sx = 0
    Sy = 0
    Sxx = 0
    Syy = 0
#    print('len(x): ', len(x), ' x: ', x, 'len(y): ', len(y), ' y: ', y)
    if len(x) * len(y) > 0:
        # x = slope*y + intercept
        meanx = np.mean(x)
        meany = np.mean(y)
        n = len(x)
        for j in range(0, len(x)):
           Sxy = Sxy + x[j]*y[j]
           Sx = Sx + x[j]
           Sy = Sy + y[j]
           Sxx = Sxx + pow(x[j],2)
           Syy = Syy + pow(y[j],2)
        
        
        if Syy - meany*Sy == 0:
#            print('Syy - meany*Sy = 0')
            slope = np.nan
            intercept = np.nan
        else:
            slope = (Sxy - meanx*Sy)/(Sxx - meanx*Sx)
            intercept = meany - slope*meanx
    
        numR = (n*Sxy - Sx*Sy)
        try:
            denR = math.sqrt(n*Sxx - pow(Sx,2))*math.sqrt(n*Syy - pow(Sy,2))
        except:
            denR = np.nan
        if denR != 0:
            R = numR/denR
        else:
            R = np.nan
        
        return R, slope, intercept
    else:
        return np.nan, np.nan, np.nan      

def fcnGetCorrelationCoefficient( x, y ):

    x = np.array(x)
    y = np.array(y)
    x = x[~np.isnan(x)]
    y = y[~np.isnan(y)]
    Sxy = 0
    Sx = 0
    Sy = 0
    Sxx = 0
    Syy = 0
    #print('len(x): ', len(x), ' x: ', x, 'len(y): ', len(y), ' y: ', y)
    if len(x) * len(y) > 0:
        # x = slope*y + intercept
        meanx = np.mean(x)
        meany = np.mean(y)
        n = len(x)
        for j in range(0, len(x)):
           Sxy = Sxy + x[j]*y[j]
           Sx = Sx + x[j]
           Sy = Sy + y[j]
           Sxx = Sxx + pow(x[j],2)
           Syy = Syy + pow(y[j],2)        
        
        if Syy - meany*Sy == 0:
#            print('Syy - meany*Sy = 0')
            slope = np.nan
            intercept = np.nan
        else:
            slope = (Sxy - meanx*Sy)/(Syy - meany*Sy)
            intercept = meanx - slope*meany
        f =[0]*len(y)
        SStot = 0
        SSres = 0
        for i in range(len(y)):
            f[i] = slope*y[i] + intercept
            SSres += (x[i] - f[i])*(x[i] - f[i])
            SStot += (x[i] - meanx)*(x[i] - meanx)
        '''  
        print('meanx ' + str(meanx) + ' SStot ' + str(SStot) + ' SSres ' + str(SSres))
        numR = (n*Sxy - Sx*Sy)
        denR = math.sqrt(n*Sxx - pow(Sx,2))*math.sqrt(n*Syy - pow(Sy,2))
        if denR != 0:
            R = numR/denR
        else:
            R = np.nan
        '''
        if SStot != 0 :
            R_squared = 1 - SSres/SStot
        else:
            R_squared = np.nan
        
        return R_squared, slope, intercept
    else:
        return np.nan, np.nan, np.nan

def fcnGetCorrelationCoefficientMinimal( x, y , debug=False):

    x = np.array(x)
    y = np.array(y)
    x = x[~np.isnan(x)]
    y = y[~np.isnan(y)]
    if debug:
        print('old', end='=')        
        for i in range(0, len(x)):
            print(f3(x[i]), end=', ')
        print('\n')
    Sxy = 0
    Sx = 0
    Sy = 0
    Sxx = 0
    Syy = 0
    #print('len(x): ', len(x), ' x: ', x, 'len(y): ', len(y), ' y: ', y)
    if len(x) * len(y) > 0:
        # x = slope*y + intercept
        meanx = np.mean(x)
        meany = np.mean(y)
        n = len(x)
        for j in range(0, len(x)):
           Sxy = Sxy + x[j]*y[j]
           Sx = Sx + x[j]
           Sy = Sy + y[j]
           Sxx = Sxx + x[j]*x[j]
           Syy = Syy + y[j]*y[j]      
        if debug:
            print('old Sx ' + f3(Sx) + ' Sy ' + f3(Sy) + ' Sxy ' + f3(Sxy) + ' n ' + str(n))
        if Syy - meany*Sy == 0:
            slope = np.nan
            intercept = np.nan
        else:
            slope = (Sxy - meanx*Sy)/(Syy - meany*Sy)
            intercept = meanx - slope*meany    
         
        return slope, intercept
    else:
        return np.nan, np.nan

#x = slope*y + intercept
def rotateLineParameters(slope, intercept, alpha):
    theta = math.atan(-slope)
    p = intercept / math.sqrt(1 + pow(slope,2))
    beta = theta + alpha
    new_slope = - math.tan(beta)
    new_intercept = p/ math.cos(beta)
    return new_slope, new_intercept