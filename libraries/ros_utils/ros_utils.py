#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 10:52:36 2019

@author: hiro
"""

import matplotlib.pyplot as plt
import math
import numpy as np
import sys
import os
dist_path = '/opt/ros/kinetic/lib/python2.7/dist-packages'
if dist_path not in sys.path:
    sys.path.append(dist_path)
if 'rospy' not in sys.modules:   
    import rosbag, rospy, rospkg
    from sensor_msgs.msg import CompressedImage
    from sensor_msgs.msg import Imu
    from nav_msgs.msg import Odometry
    from nmea_msgs.msg import Gprmc
    from nmea_msgs.msg import Gpgga
#import cv2

from collections import OrderedDict

# ROS messages format
class CompressedImageMsg:
    __slots__ = ['bgr']
    def __init__(self, msg):
        img = np.frombuffer(msg.data, dtype=np.uint8)
#        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
#        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        self.bgr = img
class Linear:
    def __init__(self,msg):
        self.x = msg.linear.x
        self.y = msg.linear.y
        self.y = msg.linear.y
class Angular:
    def __init__(self,msg):
        self.x = msg.angular.x
        self.y = msg.angular.y
        self.y = msg.angular.y
class TwistMsg:
    def __init__(self,msg):
        self.linear = Linear(msg)
        self.angular = Angular(msg)

# Probably better for plots      
        
class Lidar:
    
    def __init__(self, bagfile, topicname):
        self.time_ms = []
        bag = check_bag(bagfile) 
        scan_msgs = bag.read_messages(topics=topicname)
        self.time_ms, self.time = get_time_ms(bag, topicname)    
        lm = []
        ts = []
        for topic, msg, t in scan_msgs:
            l = msg.ranges
            lm.append(l)
            ts.append(t)
        self.ranges = lm
class Float32:
    __slots__ = ['data', \
                 'time_ms', 'time']
    def __init__(self, bagfile, topicname):
        self.data = []      
        
        bag = check_bag(bagfile) 
        self.time_ms, self.time = get_time_ms(bag,topicname)
        msgs = bag.read_messages(topics=topicname)  
        for topic, msg, t in msgs:
            self.data.append(msg.data)
        
        
class CmdVel:    
    __slots__ = ['linear_x', 'linear_y', 'linear_z', \
                 'angular_x', 'angular_y', 'angular_z', \
                 'time_ms', 'time']
    def __init__(self, bagfile, topicname):
        self.linear_x = [] 
        self.linear_y = [] 
        self.linear_z = [] 
        self.angular_x = []
        self.angular_y = []
        self.angular_z = []
        
        bag = check_bag(bagfile) 
        cmd_vel_msgs = bag.read_messages(topics=topicname)
        for topic, msg,t in cmd_vel_msgs:
            self.linear_x.append(msg.linear.x)
            self.linear_y.append(msg.linear.y)
            self.linear_z.append(msg.linear.z)
            self.angular_x.append(msg.angular.x)
            self.angular_y.append(msg.angular.y)
            self.angular_z.append(msg.angular.z)
        self.time_ms, self.time = get_time_ms(bag,topicname)
    
class Epos:
    __slots__ = ['current_time_ms', 'current_time', \
                 'fl_current_mA', 'fr_current_mA', 'rl_current_mA', 'rr_current_mA', \
                 'des_steer_pos_time_ms', 'des_steer_pos_time', \
                 'fl_des_steer_pos', 'fr_des_steer_pos', 'rl_des_steer_pos', 'rr_des_steer_pos', \
                 'cur_steer_pos_time_ms', 'cur_steer_pos_time', \
                 'fl_cur_steer_pos', 'fr_cur_steer_pos', 'rl_cur_steer_pos', 'rr_cur_steer_pos']                 

    def __init__(self, bagfile, desired_wheels_angle_topic, current_topic, steering_position_topic):
        bag = check_bag(bagfile) 
        
        current = MultiArrayMsg2arrays(bag, current_topic)
        self.current_time_ms, self.current_time = get_time_ms(bag, current_topic)
        self.fl_current_mA = np.array(current.data0)
        self.fr_current_mA = np.array(current.data1)
        self.rl_current_mA = np.array(current.data2)
        self.rr_current_mA = np.array(current.data3)
                
        desired_wheels_angle = MultiArrayMsg2arrays(bag, desired_wheels_angle_topic)
        self.des_steer_pos_time_ms, self.des_steer_pos_time = get_time_ms(bag, desired_wheels_angle_topic)
        self.fl_des_steer_pos = np.array(desired_wheels_angle.data0)
        self.fr_des_steer_pos = np.array(desired_wheels_angle.data1)
        self.rl_des_steer_pos = np.array(desired_wheels_angle.data2)
        self.rr_des_steer_pos = np.array(desired_wheels_angle.data3)
        
        steering_position = MultiArrayMsg2arrays(bag, steering_position_topic)
        self.cur_steer_pos_time_ms, self.cur_steer_pos_time = get_time_ms(bag, steering_position_topic)
        self.fl_cur_steer_pos = np.array(steering_position.data0)
        self.fr_cur_steer_pos = np.array(steering_position.data1)
        self.rl_cur_steer_pos = np.array(steering_position.data2)
        self.rr_cur_steer_pos = np.array(steering_position.data3)

class Escon:
    __slots__ = ['current_time_ms', 'current_time', \
                 'fl_current_mA', 'fr_current_mA', 'rl_current_mA', 'rr_current_mA', \
                 'pwm_time_ms', 'pwm_time', \
                 'fl_des_prop_pwm', 'fr_des_prop_pwm', 'rl_des_prop_pwm', 'rr_des_prop_pwm']
    def __init__(self, bagfile, propulsion_pwm_topic, current_topic):
        bag = check_bag(bagfile) 
        if bag.get_type_and_topic_info()[1][current_topic].msg_type == 'std_msgs/Int32MultiArray':
            current = MultiArrayMsg2arrays(bag, current_topic)
        elif bag.get_type_and_topic_info()[1][current_topic].msg_type == 'std_msgs/String':
            current = String2fourArrays(bag, current_topic)
        self.current_time_ms, self.current_time = get_time_ms(bag, current_topic)
        self.fl_current_mA = np.array(current.data0)
        self.fr_current_mA = np.array(current.data1)
        self.rl_current_mA = np.array(current.data2)
        self.rr_current_mA = np.array(current.data3)
        
        propulsion_pwm = MultiArrayMsg2arrays(bag, propulsion_pwm_topic)
        self.pwm_time_ms, self.pwm_time = get_time_ms(bag, propulsion_pwm_topic)
        self.fl_des_prop_pwm= np.array(propulsion_pwm.data0)
        self.fr_des_prop_pwm= np.array(propulsion_pwm.data1)
        self.rl_des_prop_pwm= np.array(propulsion_pwm.data2)
        self.rr_des_prop_pwm= np.array(propulsion_pwm.data3)        

class IMU_string:
    __slots__ = ['qx','qy','qz','qw','ax','ay','az','gx','gy','gz','mx','my','mz','time_ns', 'time_ms']
    def __init__(self, bagfile, topic):
        bag = check_bag(bagfile) 
        msgs = bag.read_messages(topics = topic)
        invalid  = 0
        
        self.qx=[]
        self.qy=[]
        self.qz=[]
        self.qw=[]
        self.ax=[]
        self.ay=[]
        self.az=[]
        self.gx=[]
        self.gy=[]
        self.gz=[]
        self.mx=[]
        self.my=[]
        self.mz=[]
        self.time_ms, self.time_ns = get_time_ms(bag,topic)
        for topic, msg, t in msgs:            
            aux = str(msg)
#            print(aux)
#            print(aux, len(aux), 'carriage ',aux.find('\r  \n'), aux[len(aux)-10])
            aux = aux[aux.find('qx,qy,qz,qw,ax,ay,az,gx,gy,gz,mx,my,mz')+2 : len(aux)-9]
            aux = aux.split(",")
            valid = True
#            print(aux)
            for i in range(13, 26):
                if i < len(aux):
                    if representsNum(aux[i]) is False:
                        print(i, aux[i])
                        valid = False
                else:
                    valid = False
            if valid:
                a = 13
                self.qx.append(float(aux[a+0]))
                self.qy.append(float(aux[a+1]))
                self.qz.append(float(aux[a+2]))
                self.qw.append(float(aux[a+3]))
                self.ax.append(float(aux[a+4]))
                self.ay.append(float(aux[a+5]))
                self.az.append(float(aux[a+6]))
                self.gx.append(float(aux[a+7]))
                self.gy.append(float(aux[a+8]))
                self.gz.append(float(aux[a+9]))
                self.mx.append(float(aux[a+10]))
                self.my.append(float(aux[a+11]))
                self.mz.append(float(aux[a+12]))
            else:                
                invalid +=1
        if invalid > 0:                
            print('\n\n\n\ninvalid strings: ', invalid)
            
        
class MultiArrayMsg2arrays:
    def __init__(self, bagfile, topicname):
        self.data0 = []
        self.data1 = []
        self.data2 = []
        self.data3 = []
        bag = check_bag(bagfile) 
        msgs = bag.read_messages(topics=topicname)
        for topic, msg,t in msgs:
            self.data0.append(msg.data[0])
            self.data1.append(msg.data[1])
            self.data2.append(msg.data[2])
            self.data3.append(msg.data[3])
        self.time_ms, self.time = get_time_ms(bag,topic)

class String2fourArrays:
    def __init__(self, bagfile, topicname):
        self.data0 = []
        self.data1 = []
        self.data2 = []
        self.data3 = []
        bag = check_bag(bagfile) 
        msgs = bag.read_messages(topics=topicname, raw=True)
        invalid = 0
        for topic, msg,t in msgs:
            aux = str(msg[1])
            aux = aux[aux.find('C')+2 : aux.find('\\r\\n')]
            aux = aux.split(",")
            if representsInt(aux[0]) and representsInt(aux[1]) and \
                representsInt(aux[2]) and representsInt(aux[3]):
                self.data0.append(int(aux[0]))
                self.data1.append(int(aux[1]))
                self.data2.append(int(aux[2]))
                self.data3.append(int(aux[3]))
            else:                
                invalid +=1
        if invalid > 0:                
            print('\n\n\n\ninvalid strings: ', invalid)
                


def check_bag(bagfile):
    if type(bagfile) is rosbag.bag.Bag:
        return bagfile
    elif type(bagfile) is str:
        return rosbag.Bag(bagfile)  
    else:
        return -1
    
# Returns timestamp in ms and ns           
def get_time_ms(bagfile, topicname): 
    bag = check_bag(bagfile)    
    time = get_timestamp(bag, topicname)   
    time_ms = []
    if len(time) > 0 and type(time[0]) is rospy.rostime.Time:
        time_ms = [int(t.to_sec()*1e3) for t in time]
    time= [t.to_sec()*1e9 for t in time]  
    return np.array(time_ms), np.array(time)
    
def get_timestamp(bagfile, topicname):
    bag = check_bag(bagfile)
    ts = []
    for topic, msg, t in bag.read_messages(topics=topicname, raw=True):
        ts.append(t)
    return ts

def read_images(bag_file, topicname):
    image_msgs = []

    bag = check_bag(bagfile)
    messages = bag.read_messages(topics=topicname)
    num_images = bag.get_message_count(topic_filters=topicname)
    for i in range(0,1):
        topic, msg, t  = next(messages)
        image_msgs.append(CompressedImageMsg(msg))
    return image_msgs, t 

def timestamp_info(bagfile, topics, plot_info=False):
    ts = OrderedDict()
    num_msgs = OrderedDict()
    max_ts = OrderedDict()
    min_ts = OrderedDict()
    bag = check_bag(bagfile)
    
    if type(topics) is str:
        topics = [topics]
        
    for topic in topics:
        print(topic)
        ts[topic] = get_timestamp(bag, topic)
        if len(ts[topic]) > 0:
            num_msgs[topic] = len(ts[topic])
            max_ts[topic] = max(ts[topic]).to_sec()
            min_ts[topic] = min(ts[topic]).to_sec()
        
    if plot_info:
        
        plt.figure()
        plt.subplots_adjust(hspace=0.0)
        i = 1
        for topic in topics:
            plt.subplot(2,1,1)
            plt.plot(i, min_ts[topic], 'bo')
            plt.plot(i, max_ts[topic], 'kx')
            
            plt.subplot(2,1,2) 
            plt.plot(i, num_msgs[topic], 'ro')
            plt.text(i, num_msgs[topic], str(num_msgs[topic]))
            i+=1  
        
        plt.subplot(2,1,1)
        plt.ylabel('min/max ts') 
        plt.subplot(2,1,2)
        plt.ylabel('num msgs')         
        topic_names = [str(s) for s in topics]
        plt.xticks(range(1,len(ts)+1), topic_names, rotation = 90)
        plt.subplots_adjust(bottom=0.5)
        
    return ts, num_msgs, max_ts, min_ts

def representsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
        
def representsNum(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False