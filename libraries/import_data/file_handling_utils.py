#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 14:57:07 2019

@author: hiro
"""
import os

def adjustPathNames(path):
    if path.rfind('/'):
        if path.rfind('/') == len(path) - 1:
            #print('remove last / from path ' + str(path))
            sys.exit()
        data_dir = path[0:path.rfind('/')]
        path = path.split('/')[-1]
        return path, data_dir

def getLatestMatchingFile(path, basename):
    print('getLatestMatchingFile: ' + path + ' basename: ' + basename)
    max_mtime = 0
    max_file = ''
    for dirname,subdirs,files in os.walk(path):
        for fname in files:
            full_path = os.path.join(dirname, fname)
            mtime = os.stat(full_path).st_mtime
            print('>>>' + str(full_path) + ' mtime: ' + str(mtime))
            if mtime > max_mtime and fname.find(basename) == 0:
                max_mtime = mtime
                max_dir = dirname
                max_file = full_path
    return max_file
