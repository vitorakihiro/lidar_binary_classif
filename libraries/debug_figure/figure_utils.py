#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 09:40:11 2020

@author: dell
"""
import matplotlib.pyplot as plt

def adjustFigure(fig_num=1, window_left=0, window_top=0, width=800, height=600, r=1, c=1, window_title='', hspace=1.2, wspace=0.9):

    fig = plt.figure(fig_num)
    if window_title is not '':
        fig.canvas.set_window_title(window_title)
    mngr = plt.get_current_fig_manager()
    mngr.window.setGeometry(window_left,window_top,width, height)
    fig.subplots_adjust(hspace=hspace)
    fig.subplots_adjust(wspace=wspace)
    return fig, r, c
