# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button, RadioButtons
import numpy as np
import math
import sys, os

from cam_utils import *

from ros_utils import *
#from ros_utils.dist_packages import rosbag, rospy, rospkg
from tictoc import *

NOMINAL_LIDAR_RATE = 0.025

#import cv2
#from perception_utils import *
#from rosbag_utils import *
#from aux2plot import *

font = {'family': 'serif',
    'color':  'black',
    'weight': 'normal',
    'size': 16,
    }
font14 = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 14,
        }
font_sp = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 8,
        }

def f3(val):
    return format(val,'3.3f')
class DebugFigure:  

    def __init__(self, plots_fcn=None):
        self.plots = plots_fcn
        self.break_for = False
    def setOnClick(self, fcn=None):
        self.onClick = fcn

    def initFigure(self, fig_num=1, figsize=[15,8], \
                   custom_keys=False, \
                   custom_press=None, \
                   rows=1, cols=1,\
                   reference_time_seq=[],\
                   subplots_hspace=0.5, subplots_wspace=1, \
                   window_left=100, window_top=100, \
                   window_width=1000, window_height=500):
        self.fig = plt.figure(fig_num, figsize)
        if custom_keys:
            self.fig.canvas.mpl_disconnect(self.fig.canvas.manager.key_press_handler_id)
            if custom_press:
                self.fig.canvas.mpl_connect('key_press_event', custom_press)
            else:    
                self.fig.canvas.mpl_connect('key_press_event', self.press)
        self.reference_time_seq = reference_time_seq
        if len(reference_time_seq) > 0:
            self.ref_time = reference_time_seq[0]
        else:
            self.ref_time = 0
        self.fig.subplots_adjust(hspace=subplots_hspace)
        self.fig.subplots_adjust(wspace=subplots_wspace)
        self.mngr = plt.get_current_fig_manager()
        self.mngr.window.setGeometry(window_left,window_top,\
                                     window_width, window_height) 
        self.gs = gridspec.GridSpec(rows,cols)      
    class Markers:
        def __init__(self, marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5):
            self.type = marker
            self.color = marker_color
            self.size = marker_size
            self.update_type = update_marker
            self.update_color = update_color
            self.update_size = update_marker_size
    class Arrow:
        def __init__(self, fig=None, gs=[], \
                     x=[], y=[], timestamp=[], heading=[],\
                     color='r', width=0.01,size=0.1, \
                     _ax=None):
            
            self.fig = fig
            self.gs=gs
            self.x=x
            self.y=y
            self.timestamp=timestamp
            self.heading=heading
            self.color=color
            self.width=width            
            self.size=size
            
            if _ax == None:
                self.ax = self.fig.add_subplot(gs)
            else:
                self.ax = _ax
            
            self.arrow = self.ax.arrow(x[0],y[0],size*math.cos(heading[0]),size*math.sin(heading[0]),\
                                       color=color, width=width)
            
        def update(self,k=0, ts_to_find=None):
            
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0] 
            print('Arrow ts_to_find:', ts_to_find, ' found: ', self.timestamp[k], 'diff:', ts_to_find-self.timestamp[k]  )
            self.arrow.remove()
            self.arrow = self.ax.arrow(self.x[k],self.y[k],\
                       self.size*math.cos(self.heading[k]),self.size*math.sin(self.heading[k]), \
                       color=self.color, width=self.width)
    class Camera:
        def __init__(self, fig=None, gs=[], \
                     alias = '', \
                     bag=None, topic='/cam', \
                     ts_cam=None, \
                     plot_visible=True, title='', _ax=None):
            self.plot_visible = plot_visible
            self.ts_cam = ts_cam
            self.alias = alias
            if _ax == None:
                self.ax = fig.add_subplot(gs)
            else:
                self.ax = _ax  
                
            frame, diff = ts_cam.getFrame(0,False)
            self.im = self.ax.imshow(frame)
            self.ax.axis('off')
            self.ax.set_title('')
            
        
        def update(self, ref_ts_index):
            frame, diff = self.ts_cam.getFrame(ref_ts_index,False)
            th = self.alias + ' diff[ms]: ' + str(diff) + 'ms'
            print(th)
            self.im.set_data(frame)
            self.ax.set_title(th, fontdict=font_sp)
            
            
                        
    class LiDAR:
        def __init__(self, fig=None, gs=[], \
                    bag=None, topic='/scan', \
                    angle=[], lidar=[], angle_offset=0,timestamp=[], idx=0,\
                    marker_color='k', marker='.', marker_size=0.5, \
                    origin_marker_size=5,\
                    rotate_degrees=0,\
                    second_unit=1,\
                    xaxis_min=-1, xaxis_max=1, yaxis_min=-0.5, yaxis_max=1.5,\
                    xlabel_on=True, ylabel_on=True, 
                    lidar_index_begin=0, lidar_index_end=None, \
                    lidar_min_dist_m=0, lidar_max_dist_m=10, \
                    plot_visible=True, title='', _ax=None):
            self.k = 0
            self.second_unit = second_unit#timestamp in seconds
            self.index = 0
            if bag != None:
                self.bag = bag
                self.topic = topic
                self.msgs = bag.read_messages(topics=topic)
                self.msg_cnt = bag.get_message_count(topic)
                msg = next(self.msgs)
                self.index = 0
                self.l_ts_ms, l_ts_ns = get_time_ms(bag, topic)
                self.l_ts = l_ts_ns*1e-9
                l = msg.message.ranges
                a_min = msg.message.angle_min + rotate_degrees*math.pi/180
                a_max = msg.message.angle_max + rotate_degrees*math.pi/180
                d_a = msg.message.angle_increment
                self.angle=np.linspace(a_min, a_max,\
                    int ((a_max-a_min)/d_a + 1) )   
            self.plot_visible = plot_visible
            self.rotate_degrees = rotate_degrees
            if len(lidar) > 0:
                self.bag = None
                self.lidar = lidar
                self.angle = angle
                self.l_ts = np.array(timestamp)
                l = lidar[self.l_ts[idx]]
            if (self.l_ts[-1] - self.l_ts[0])/len(self.l_ts) > 1000:
                self.second_unit = 1000

            self.angle_offset = angle_offset
            
            if lidar_index_end == None:
                lidar_index_end = len(self.angle) - 1 
            self.lidar_index_begin= lidar_index_begin
            self.lidar_index_end = lidar_index_end
            self.lidar_min_dist_m = lidar_min_dist_m
            self.lidar_max_dist_m = lidar_max_dist_m
            x = []
            y = []
            if np.mean(l) > 30:
                factor = 1000
            else:
                factor = 1
            self.factor=factor
            
            if _ax == None:
                self.ax = fig.add_subplot(gs)
            else:
                self.ax = _ax  
            print('len l:', len(l), 'len angle:', len(self.angle))                 
            for i in range(1,len(l)):
                if l[i] > lidar_min_dist_m*factor and l[i] < lidar_max_dist_m*factor and \
                    i >= lidar_index_begin and i <= lidar_index_end :
                    x.append(l[i]/factor*math.cos(self.angle[i] - angle_offset))
                    y.append(l[i]/factor*math.sin(self.angle[i] - angle_offset))            
            self.pl, = self.ax.plot(x,y,\
                    color=marker_color,marker=marker,ms=marker_size, \
                    visible = plot_visible)
            self.pl.set_linewidth(0)
            self.ax.plot([0],[0],'rx',ms=origin_marker_size, visible = plot_visible)    
            self.ax.axis([xaxis_min,xaxis_max,yaxis_min,yaxis_max]) 
            
            if xlabel_on:
                self.ax.set_xlabel('x [m]', fontdict=font14)
            if ylabel_on:
                self.ax.set_ylabel('y [m]', fontdict=font14)
            self.ax.set_aspect('equal')
        
            self.ax.set_title(title,fontdict=font_sp)
            
        def update(self, k=1, rotate_degrees=0, angle_offset=0, plot_visible=True, title_str=''):
            x = []
            y = []
            prev_ts = self.l_ts[self.index]
            self.rotate_degrees = rotate_degrees
            
            if isinstance(k, int):
                #if k is int, it's probably the timestamp vector index
                required_ts = self.l_ts[k]
            else:
                required_ts = k
            print('required_ts', required_ts)
            index_ts_greater_than_required = np.where(self.l_ts>=required_ts)[0]
            if len(index_ts_greater_than_required) > 0 :                        
                self.k = index_ts_greater_than_required[0]    
#            print(index_ts_greater_than_required)
#            print('updating k: ', k, 'self.k:', self.k)
#            print('self.k:', self.k, 'self.index:',self.index )
            print('prev_ts:', prev_ts, 'self.l_ts[k]', self.l_ts[self.k], 'size(l_ts):', len(self.l_ts), 'req: ', self.l_ts[self.k], 'lidar size:', len(self.lidar)  )
            
            if self.bag == None:
                # lidar probably came from txt file 
                l = self.lidar[self.l_ts[self.k]]
                self.index = self.k
            else:
                if self.k >= self.index+1 and self.k < self.msg_cnt and \
                    abs(self.l_ts[self.k] - required_ts) < NOMINAL_LIDAR_RATE*self.second_unit:
                    # index is greater than previous one
                    for i in range(0,self.k-self.index+1):
                        msg = next(self.msgs)
                    l = msg.message.ranges
                    self.index = self.k
                    
                else:
                    # previous index is greater than required index. It requires
                    # reading messages from the beginning as the container self.msgs
                    # only goes forward
                    self.msgs = self.bag.read_messages(topics=self.topic)
                    msg = next(self.msgs)
                    for i in range(1, self.k+1):
                        msg = next(self.msgs)
                        if abs(required_ts-self.l_ts[i]) < 0.1*abs(required_ts-self.l_ts[i-1]) or \
                            abs(required_ts-self.l_ts[i]) < NOMINAL_LIDAR_RATE*self.second_unit or \
                            i == self.k:
                            l = msg.message.ranges
#                            print('Found', i, 'self.index:',self.index, \
#                                  'l_ts:', self.l_ts[self.index])
#                            print('required_ts:', required_ts, 'self.l_ts[i]:', self.l_ts[i], \
#                                  'self.l_ts[i-1]:', self.l_ts[i-1])
                            self.index = i
                            break
            # When lidar readings come from txt files, they may be in mm units
            # rather than in meter (case of rosbag files). self.factor handles
            # these two situations to keep coordinates x, y in meters
            if np.mean(l) > 30:
                factor = 1000
            else:
                factor = 1
            self.factor=factor
            for i in range(1,len(l)):
                if l[i] > self.lidar_min_dist_m*self.factor and l[i] < self.lidar_max_dist_m*self.factor and \
                    i >= self.lidar_index_begin and i <= self.lidar_index_end :
                    x.append(l[i]/self.factor*math.cos(self.angle[i] + self.rotate_degrees*math.pi/180 - angle_offset))
                    y.append(l[i]/self.factor*math.sin(self.angle[i] + self.rotate_degrees*math.pi/180 - angle_offset))
            self.pl.set_ydata(y)
            self.pl.set_xdata(x) 
            self.pl.set_visible(plot_visible) 
            if title_str == '':
                th = 'Scan ts: ' + str(self.l_ts[self.k]) + ' ms\n' + \
                    f3(0.001*(self.l_ts[self.k]-self.l_ts[0])) + '/' + \
                    f3(0.001*(self.l_ts[len(self.l_ts)-1]-self.l_ts[0])) + 's'
            else:
                th = title_str
            self.ax.set_title(th,fontdict=font_sp)
            
        def crossTimestampCheck(self, required_ts=0, add_str='', always_print=False ):
            if abs(required_ts-self.l_ts[self.index]) > NOMINAL_LIDAR_RATE or \
                always_print:
                print(add_str)
                print('diff[i-1]:',abs(required_ts-self.l_ts[self.index-1]))
                print('diff[i]:',abs(required_ts-self.l_ts[self.index]))
                print('diff[i+1]:',abs(required_ts-self.l_ts[self.index+1]))
        '''  

        def updateLine(pl, ax, slope, intercept, plot_visible=True):
            
            bottom, top = ax.get_ylim()
            y = np.array([bottom, top])
            x = slope*y+intercept
            pl.set_ydata(y)
            pl.set_xdata(x)
            pl.set_visible(plot_visible) 
        '''      


    class Line:
        def __init__(self, fig=None, gs=[], slope=[0], intercept=[0], intercept_x=[0], timestamp=[], idx = 0, color='k', marker='.', linestyle='--', linewidth=1, _ax=None, plot_visible=True):
            self.slope = slope
            self.intercept = intercept
            self.intercept_x = intercept_x
            self.timestamp = timestamp
            if _ax == None:
                self.ax = self.fig.add_subplot(gs)
            else:
                self.ax = _ax
            bottom, top = self.ax.get_ylim()
            y = np.array([bottom, top])

            if len(intercept_x) > 1: #vertical line
                x = [intercept_x[idx], intercept_x[idx]]
            else:                
                x = slope[idx]*y+intercept[idx]
            self.pl, = self.ax.plot(x, y, color=color, marker=marker,\
                        linestyle=linestyle, linewidth=linewidth, \
                        visible=plot_visible)
        def update(self, k= 1, ts_to_find=None, title_str='', plot_visible=True):
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
                print('k', k, 'ts_to_find', ts_to_find, 'x')

            bottom, top = self.ax.get_ylim()
            y = np.array([bottom, top])

            if len(self.intercept_x) > 1: #vertical line
                x = [self.intercept_x[k], self.intercept_x[k]]
            else:
                x = self.slope[k]*y+self.intercept[k]
            self.pl.set_ydata(y)
            self.pl.set_xdata(x)
            self.pl.set_visible(plot_visible) 
     

    class Polar:
        def __init__(self, fig=None, gs=[], angle_rad=[], ray=[], marker='.', _ax=None,xlim=True, theta_min=0,theta_max=180, r_max=-1):
            self.fig = fig
            self.ray = ray

            if _ax == None:
                self.ax = self.fig.add_subplot(gs,polar=True)
            else:
                self.ax = _ax
            if r_max == -1:
                r_max = max(ray)
            self.pl, = self.ax.plot(angle_rad,ray,marker,ms=0.5)
            self.ax.set_thetamin(theta_min)
            self.ax.set_thetamax(theta_max)
            self.ax.set_rmax(r_max)
        def update(self, k=1, ray=[]):
            self.pl.set_ydata(ray)

    class XY_line:
        def __init__(self, fig=None, gs=[],  x0=[], y0=[], \
                     x1=[], y1=[], timestamp=[], idx = 0,\
                     marker='x', marker_color='b', marker_size=1.4, \
                     linecolor='b', linewidth=1, linestyle='--',\
                     _xticks=True, _yticks=True, plot_visible=True,\
                     _ax=None, xlim=True,title_str=''):
            self.x0 = x0
            self.y0 = y0
            self.x1 = x1
            self.y1 = y1
            self.timestamp=[]
            self.fig = fig
            
            if _ax == None:
                self.ax = self.fig.add_subplot(gs)
            else:
                self.ax = _ax

            self.pl, = self.ax.plot([x0[idx],x1[idx]], [y0[idx],y1[idx]], \
                                 marker=marker, ms=marker_size,\
                                 markeredgecolor=marker_color, \
                                 linewidth=linewidth, color=linecolor,\
                                 linestyle=linestyle, visible=plot_visible)
            
            if not _xticks:
                self.ax.xticks([]) 
            if not _yticks:
                self.ax.yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str)                
        def update(self, k=1, ts_to_find=None, plot_visible=True, title_str=''):
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
                print('k', k, 'ts_to_find', ts_to_find, 'x', self.x0[k])
                    
            self.pl.set_ydata([self.y0[k],self.y1[k]])
            self.pl.set_xdata([self.x0[k],self.x1[k]])
            self.pl.set_visible(plot_visible)
            if title_str != '':
                self.ax.set_title(title_str, fontdict=font_sp) 
        def updateValues(self, x0, y0, x1, y1):
            self.x0 = x0
            self.y0 = y0
            self.x1 = x1
            self.y1 = y1
                   
    class XY_full: #supply the required (x,y) for each update/ no timestamp suport
        def __init__(self, fig=None, gs=[],  x=[], y=[],\
                     marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5, \
                     linewidth=1, \
                     _xticks=True, _yticks=True,\
                     _ax=None, xlim=True,title_str=''):
            self.x = x
            self.y = y
            self.fig = fig
            left = min(x) if len(x) > 0 else 0
            right = max(x) if len(x) > 0 else 0 
            if _ax == None:
                self.ax = self.fig.add_subplot(gs)
            else:
                self.ax = _ax
                left, right = self.ax.get_xlim()

            self.pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                  markeredgecolor=marker_color, \
                                  color=marker_color, linewidth=linewidth)                
            
            if xlim:
                if left > min(x):
                    left = min(x)
                if right < max(x):
                    right = max(x)
                self.ax.set_xlim(left,right)
            if not _xticks:
                self.ax.xticks([]) 
            if not _yticks:
                self.ax.yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str)
                
        def update(self, x=[], y=[], title_str=''):
            self.pl.set_ydata(y)
            self.pl.set_xdata(x)
            if title_str != '':
                self.ax.set_title(title_str, fontdict=font_sp) 
            
    class XY_point:

        def __init__(self, fig=None, gs=[],  x=[], y=[], timestamp=[], idx=0,\
                     marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5, \
                     linewidth=1, focus=False, windows_size=10,\
                     _xticks=True, _yticks=True, plot_visible=True,\
                     _ax=None, xlim=True,title_str=''):
            if len(x) == 0:
                x = [0]
                y = [0]
            self.x = x
            self.y = y
            self.timestamp = np.array(timestamp)
            self.fig = fig
            left = min(x)
            right = max(x)
            
            if _ax == None:
                self.ax = self.fig.add_subplot(gs)
                self.main_ax = True
                self._pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                     markeredgecolor=marker_color, \
                                     color=marker_color, linewidth=linewidth, visible=plot_visible)
            else:
                self.ax = _ax
                self.main_ax = False
                self._pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                     markeredgecolor=marker_color, \
                                     color=marker_color, linewidth=linewidth, visible=plot_visible)
                left, right = self.ax.get_xlim()
                
            self.pl, = self.ax.plot(x[0],y[0],marker=update_marker,markeredgecolor=update_color,\
                           ms=update_marker_size, linewidth=linewidth, visible=plot_visible)
            
            if xlim:
                if left > min(x):
                    left = min(x)
                if right < max(x):
                    right = max(x)
                self.ax.set_xlim(left,right)
            if focus:     
                self.adjustLim(idx, windows_size)           
            if not _xticks:
                self.ax.xticks([]) 
            if not _yticks:
                self.ax.yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str)
                
        def adjustLim(self, idx=0, windows_size=10):
            idx_before = int(idx - 0.5*windows_size)
            idx_after = int(idx + 0.5*windows_size)
            if idx_before < 0:
                idx_before = 0
            if idx_after >= len(self.x):
                idx_after = len(self.x) - 1

            if math.isnan(self.x[idx_before]*self.x[idx_after]*self.y[idx_before]*self.y[idx_after]) is False : 
                left_limit = self.x[idx_before]
                right_limit = self.x[idx_after]
                top_limit = max(self.y[idx_before:idx_after])
                bottom_limit = min(self.y[idx_before:idx_after])

                if self.main_ax is False:
                    _top, _bottom = self.ax.get_ylim()
                    _left, _right = self.ax.get_xlim()
                    if top_limit < _top:
                        top_limit = _top
                    if bottom_limit > _bottom:
                        bottom_limit = _bottom
                    if left_limit > _left:
                        left_limit = _left
                    if right_limit < _right:
                        right_limit = _right

                self.ax.set_xlim([left_limit, right_limit])
                self.ax.set_ylim([bottom_limit, top_limit])

        def update(self, k=1, x=None, y=None, ts_to_find=None, focus=False, windows_size=10, plot_visible=True, title_str=None):
            
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
            if x is not None:
                self.x = x
            if y is not None:
                self.y = y
            self.pl.set_ydata(self.y[k])
            self.pl.set_xdata(self.x[k]) 
            self.pl.set_visible(plot_visible)
            self._pl.set_visible(plot_visible)
            if focus:  
                self.adjustLim(k, windows_size)   
            if title_str != None:
                if title_str != '':
                    title_str += f3(self.y[k])
                    self.ax.set_title(title_str, fontdict=font_sp) 
                else:
                    self.ax.set_title(f3(self.y[k]), fontdict=font_sp)

    def initWidgets(self, slider_size=1):
        self.axcolor = 'lightgoldenrodyellow'
        self.axscan = plt.axes([0.15, 0.1, 0.72, 0.03], facecolor=self.axcolor)
        self.ax_btn_next = plt.axes([0.53, 0.05, 0.34, 0.03], facecolor=self.axcolor)
        self.ax_btn_prev = plt.axes([0.15, 0.05, 0.34, 0.03], facecolor=self.axcolor)
        self.slider_scan = Slider(self.axscan, 'Scan', 1, slider_size, valinit = 0, valstep=1)
        self.button_next = Button(self.ax_btn_next,'>', color=self.axcolor, hovercolor='0.975')    
        self.button_prev = Button(self.ax_btn_prev,'<', color=self.axcolor, hovercolor='0.975')
        self.button_next.on_clicked(self.next_scan)  
        self.button_prev.on_clicked(self.prev_scan)          
        self.slider_scan.on_changed(self.update)        
              
    def press(self,event):      
        if event.key == 's':
            self.break_for = True  
        elif event.key == 'right':
            print('###################### next key')
            self.slider_scan.set_val(self.slider_scan.val + 1)
            #self.update()
            
        elif event.key == 'left':
            print('###################### key')
            self.slider_scan.set_val(self.slider_scan.val - 1)
            #self.update()
        
        self.fig.canvas.draw()     

    def next_scan(self,event):
        print('###################### next button')
        a = self.slider_scan.val + 1
        self.slider_scan.set_val(a)
      
    def prev_scan(self,event):
        print('###################### prev button')
        a = self.slider_scan.val - 1
        self.slider_scan.set_val(a)
         
    def update(self, val):
        print('###################### slider')
        self.plots(int(self.slider_scan.val))
        self.fig.canvas.draw_idle()  