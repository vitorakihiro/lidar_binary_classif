# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.widgets import Slider, Button, RadioButtons
import numpy as np
import math
import sys, os
import pandas
import display

from cam_utils import *

from ros_utils import *
#from ros_utils.dist_packages import rosbag, rospy, rospkg
from tictoc import *

NOMINAL_LIDAR_RATE = 0.025
DEBUG_WIDGETS = False
DEBUG_UPDATE = True

font = {'family': 'serif',
    'color':  'black',
    'weight': 'normal',
    'size': 16,
    }
font14 = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 14,
        }
font_sp = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 8,
        }

def f3(val):
    if float(val).is_integer():
        return '{:,}'.format(int(val))    
    return '{:,.3f}'.format(val)

def find_matching_timestamp (ref_timestamp=[], timestamp=[], ref_index=0, prev_ref_index=0, last_known_matching_index=0):
    not_found = True
    if ref_index > prev_ref_index:
        k = last_known_matching_index 
    else:
        k = 0

    while not_found and k < len(timestamp)-1:
        if timestamp[k] > ref_timestamp[ref_index]: 
            not_found = False
            k -= 1
        elif timestamp[k]<=ref_timestamp[ref_index] and timestamp[k+1]>ref_timestamp[ref_index]:
            not_found = False
            k -= 1
        k += 1   
    if k < 0:
        k = 0
    return k

def adjustFigure(fig_num=1, window_left=0, window_top=0, width=800, height=600, r=1, c=1):

    fig = plt.figure(fig_num)
    mngr = plt.get_current_fig_manager()
    mngr.window.setGeometry(window_left,window_top,width, height)
    fig.subplots_adjust(hspace=1.2)
    fig.subplots_adjust(wspace=.9)
    return fig, r, c
def get_safe_x_lims(left, right):
    left_limit = 0
    right_limit = 1
    is_left_a_vec = type(left) == np.ndarray or type(left) == list or type(left) == pandas.core.series.Series
    is_right_a_vec = type(right) == np.ndarray or type(right) == list or type(right) == pandas.core.series.Series
    if type(left) == int and type(right) == int:
        if not np.isnan(left):
            left_limit = left
        if not np.isnan(right):
            right_limit = right
    elif is_left_a_vec and is_right_a_vec:
        if np.sum(~np.isnan(left)) > 0:
            left_limit = np.nanmin(left)
        if np.sum(~np.isnan(right)) > 0:
            right_limit = np.nanmax(right)
    if left_limit == right_limit > 0:
        left_limit = 0
    elif left_limit == right_limit <= 0:
        right_limit = 1

    return left_limit, right_limit

def get_safe_y_lims(top, bottom):
    top_limit, bottom_limit = get_safe_x_lims(top, bottom)
    return top_limit, bottom_limit

def rotate(x, y, angle):
    #R = [cos(th) -sin(th); sin(th) cos(th)]
    if type(x) == np.ndarray or type(x) == list or type(x) == pandas.core.series.Series:
        x_rot=[]
        y_rot=[]
        for i in range(0, len(x)):
            x_rot.append(x[i]*math.cos(angle) - y[i]*math.sin(angle))
            y_rot.append(x[i]*math.sin(angle) + y[i]*math.cos(angle))
        x_rot=np.array(x_rot)
        y_rot=np.array(y_rot)
    else:
        x_rot = x*math.cos(angle) - y*math.sin(angle)
        y_rot = x*math.sin(angle) + y*math.cos(angle)
    return x_rot, y_rot

class DebugFigure:  

    def __init__(self, plots_fcn=None):
        self.plots = plots_fcn
        self.break_for = False

    def __sizeof__(self):
        return object.__sizeof__(self) + sum(sys.getsizeof(v) for v in self.__dict__.values())
    def setOnClick(self, fcn=None):
        self.onClick = fcn

    def initFigure(self, fig_num=1, figsize=[15,8], \
                   custom_keys=False, \
                   custom_press=None, \
                   reference_time_seq=[],\
                   subplots_hspace=0.5, subplots_wspace=1, \
                   window_left=100, window_top=100, \
                   window_width=1000, window_height=500, display=None, display_num=0):
        self.fig = plt.figure(fig_num, figsize)
        if custom_keys:
            self.fig.canvas.mpl_disconnect(self.fig.canvas.manager.key_press_handler_id)
            if custom_press:
                self.fig.canvas.mpl_connect('key_press_event', custom_press)
            else:    
                self.fig.canvas.mpl_connect('key_press_event', self.press)
        self.reference_time_seq = reference_time_seq
        if len(reference_time_seq) > 0:
            self.ref_time = reference_time_seq[0]
        else:
            self.ref_time = 0
        self.fig.subplots_adjust(hspace=subplots_hspace)
        self.fig.subplots_adjust(wspace=subplots_wspace)
        self.mngr = plt.get_current_fig_manager()
        if display is not None:
            if display.width[display_num] < window_width:
                window_width = display.width[display_num] 
            if display.height[display_num] < window_height:
                window_height = display.height[display_num]
            window_left += display.left[display_num]
        print('window left ' + str(window_left) + ' top ' + str(window_top) + ' width ' + str(window_width) + ' height ' + str(window_height))
        self.mngr.window.setGeometry(window_left,window_top,\
                                     window_width, window_height)     
    class Markers:
        __slots__ = ['color', 'size', 'update_type', 'update_color', 'update_size']
        def __init__(self, marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5):
            self.color = marker_color
            self.size = marker_size
            self.update_type = update_marker
            self.update_color = update_color
            self.update_size = update_marker_size
    class Arrow:
        __slots__ = ['color', 'size', 'fig', 'x', 'y', 'timestamp', 'idx', 'heading', 'width', 'ax', 'arrow']
        #rect = [left, bottom, width, height]. All quantities are in fractions of figure width and height  
        def __init__(self, fig=None, rect=[], \
                     x=[], y=[], timestamp=[], idx=0, heading=[],\
                     color='r', width=0.01, size=0.1, \
                     _ax=None):
            
            self.fig = fig
            self.x = x
            self.y = y
            self.timestamp = timestamp
            self.idx = idx
            self.heading = heading
            self.color = color
            self.width = width            
            self.size = size
            
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
            else:
                self.ax = _ax
            
            self.arrow = self.ax.arrow(x[idx],y[idx],size*math.cos(heading[idx]),size*math.sin(heading[idx]),\
                                       color=color, width=width)
            
        def update(self, k=0, ts_to_find=None):
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0] 
            self.idx = k 
            if DEBUG_UPDATE:
                print('Arrow ts_to_find:', ts_to_find, ' found: ', self.timestamp[k], 'diff:', ts_to_find-self.timestamp[k]  )
            self.arrow.remove()
            self.arrow = self.ax.arrow(self.x[k],self.y[k],\
                       self.size*math.cos(self.heading[k]),self.size*math.sin(self.heading[k]), \
                       color=self.color, width=self.width)
    class Camera:
        __slots__ = ['ts_cam', 'alias', 'fig', 'x', 'y', 'timestamp', 'idx', 'width', 'ax', 'im', 'title_fontsize']
        def __init__(self, fig=None, rect=[], \
                     alias = '', idx=0, \
                     bag=None, topic='/cam', \
                     ts_cam=None, title_str='', title_fontsize=8, _ax=None):
            self.ts_cam = ts_cam
            self.alias = alias
            self.idx = idx
            self.title_fontsize = title_fontsize
            if _ax == None:
                self.ax = fig.add_axes(rect)
            else:
                self.ax = _ax  
                
            frame, diff = ts_cam.getFrame(0,False)
            self.im = self.ax.imshow(frame)
            self.ax.axis('off')
            self.ax.set_title('')            
        
        def update(self, ref_ts_index):
            self.idx = ref_ts_index
            frame, diff = self.ts_cam.getFrame(ref_ts_index,False)
            th = self.alias + ' diff[ms]: ' + str(diff) + 'ms'
            if DEBUG_UPDATE:
                print(th)
            self.im.set_data(frame)
            self.ax.set_title(th, fontdict={'fontsize':self.title_fontsize})
            
    class Cameras:
        __slots__ = ['ts_cam', 'alias', 'fig', 'x', 'y', 'timestamp', 'idx', 'width', 'ax', 'im', 'names', 'current_main_cam_index', 'names', 'title_fontsize']
        def __init__(self, fig=None, rect=[], \
                     alias = '', idx=[0], \
                     bag=None, names=['cam'], \
                         main_cam='cam',
                     ts_cams=[None], title_str='', title_fontsize=8, _ax=None):
            self.ts_cam = OrderedDict()
            self.names = names
            self.current_main_cam_index = names.index(main_cam)
            self.title_fontsize = title_fontsize
            for name, cam in zip(names, ts_cams):
                self.ts_cam[name] = cam
            self.alias = alias
            self.idx = idx
            idx = self.idx[self.current_main_cam_index]
            if _ax == None:
                self.ax = fig.add_axes(rect)
            else:
                self.ax = _ax                  
            frame, diff = self.ts_cam[main_cam].getFrame(idx, False)
            self.im = self.ax.imshow(frame)
            self.ax.axis('off')
            self.ax.set_title('')
            
        def change_cam(self, ref_ts_index):
            self.current_main_cam_index +=1
            self.current_main_cam_index %= len(self.names)
            self.update(ref_ts_index)

        def update(self, ref_ts_index):
            self.idx[self.current_main_cam_index] = ref_ts_index
            frame, diff = self.ts_cam[self.names[self.current_main_cam_index]].getFrame(ref_ts_index,False)            
            th = self.names[self.current_main_cam_index] + ' diff[ms]: ' + str(diff) + 'ms'
            self.im.set_data(frame)
            self.ax.set_title(th, fontdict={'fontsize':self.title_fontsize})          
                        
    class LiDAR:
        __slots__ = ['fig', 'idx', 'second_unit', 'k', 'index', 'bag', 'topic', 'msgs', 'msg_cnt', 'l_ts_ms', 'l_ts', 'angle', 'plot_visible', 'rotate_degrees', 'lidar', 'angle_offset', 'lidar_index_begin', 'lidar_index_end', 'lidar_min_dist_m', 'lidar_max_dist_m', 'factor', 'ax', 'pl', 'title_fontsize']
        def __init__(self, fig=None, rect=[], \
                    bag=None, topic='/scan', \
                    angle=[], lidar=[], angle_offset=[], timestamp=[], idx=0,\
                    marker_color='k', marker='.', marker_size=0.5, \
                    origin_marker_size=5,\
                    rotate_degrees=0,\
                    second_unit=1,\
                    xaxis_min=-1, xaxis_max=1, yaxis_min=-0.5, yaxis_max=1.5,\
                    xlabel_on=True, ylabel_on=True, 
                    lidar_index_begin=0, lidar_index_end=None, \
                    lidar_min_dist_m=0, lidar_max_dist_m=100, \
                    plot_visible=True, title_str='', title_fontsize=8, label='', _ax=None):
            self.idx = 0
            self.k = 0
            self.second_unit = second_unit#timestamp in seconds
            self.index = 0
            self.title_fontsize = title_fontsize
            if bag != None:
                self.bag = bag
                self.topic = topic
                self.msgs = bag.read_messages(topics=topic)
                self.msg_cnt = bag.get_message_count(topic)
                msg = next(self.msgs)
                self.index = 0
                self.l_ts_ms, l_ts_ns = get_time_ms(bag, topic)
                self.l_ts = l_ts_ns*1e-9
                l = msg.message.ranges
                a_min = msg.message.angle_min + rotate_degrees*math.pi/180
                a_max = msg.message.angle_max + rotate_degrees*math.pi/180
                d_a = msg.message.angle_increment
                self.angle=np.linspace(a_min, a_max,\
                    int ((a_max-a_min)/d_a + 1) )   
            self.plot_visible = plot_visible
            self.rotate_degrees = rotate_degrees
            if len(lidar) > 0:
                self.bag = None
                self.lidar = lidar
                self.angle = angle
                self.l_ts = np.array(timestamp)
                l = lidar[self.l_ts[idx]]
            if (self.l_ts[-1] - self.l_ts[0])/len(self.l_ts) > 1000:
                self.second_unit = 1000
            if len(angle_offset) == 0:
                angle_offset = [0]*len(self.lidar)
            self.angle_offset = angle_offset
            
            if lidar_index_end == None:
                lidar_index_end = len(self.angle) - 1 
            self.lidar_index_begin= lidar_index_begin
            self.lidar_index_end = lidar_index_end
            self.lidar_min_dist_m = lidar_min_dist_m
            self.lidar_max_dist_m = lidar_max_dist_m
            x = []
            y = []
            if np.mean(l) > 30:
                factor = 1000
            else:
                factor = 1
            self.factor=factor
            
            if _ax == None:
                self.ax = fig.add_axes(rect)
            else:
                self.ax = _ax  
            #print('len l:', len(l), 'len angle:', len(self.angle))    
            # 
            #print('angle len ' + str(len(angle)) + ' l ' + str(len(l)))  

            init_idx = 0
            if l[0] > 100*np.mean(l):
                init_idx = 1

            for i in range(init_idx,len(l)):
                if l[i] > lidar_min_dist_m*factor and l[i] < lidar_max_dist_m*factor and \
                    i >= lidar_index_begin and i <= lidar_index_end :
                    x.append(l[i]/factor*math.cos(self.angle[i]) + rotate_degrees*math.pi/180 )
                    y.append(l[i]/factor*math.sin(self.angle[i]) + rotate_degrees*math.pi/180 )            
            self.pl, = self.ax.plot(x,y,\
                    color=marker_color,marker=marker,ms=marker_size, \
                    visible = plot_visible, fillstyle='left', label=label)
            self.pl.set_linewidth(0) 

            self.ax.axis([xaxis_min,xaxis_max,yaxis_min,yaxis_max]) 
            print('lidar_index_begin ' + str(lidar_index_begin) + ' lidar_index_end ' + str(lidar_index_end) +  ' x ' + str(len(x)) + ' pl.x ' + str(len(self.pl.get_xdata())) + ' lidar_min_dist_m ' + str(lidar_min_dist_m) + ' lidar_max_dist_m ' + str(lidar_max_dist_m) )
            if xlabel_on:
                self.ax.set_xlabel('x [m]', fontdict=font14)
            if ylabel_on:
                self.ax.set_ylabel('y [m]', fontdict=font14)
            self.ax.set_aspect('equal')
        
            self.ax.set_title(title_str, fontdict={'fontsize': self.title_fontsize})
            
        def update(self, k=1, rotate_degrees=0, plot_visible=True, title_str=''):
            x = []
            y = []
            prev_ts = self.l_ts[self.index]
            self.rotate_degrees = rotate_degrees
            
            if isinstance(k, int):
                #if k is int, it's probably the timestamp vector index
                required_ts = self.l_ts[k]
            else:
                required_ts = k
            if DEBUG_UPDATE:
                print('required_ts', required_ts)
            index_ts_greater_than_required = np.where(self.l_ts>=required_ts)[0]
            if len(index_ts_greater_than_required) > 0 :                        
                self.k = index_ts_greater_than_required[0]    
            self.idx = self.k
            if self.bag == None:
                # lidar probably came from txt file 
                l = self.lidar[self.l_ts[self.k]]
                self.index = self.k
            else:
                if self.k >= self.index+1 and self.k < self.msg_cnt and \
                    abs(self.l_ts[self.k] - required_ts) < NOMINAL_LIDAR_RATE*self.second_unit:
                    # index is greater than previous one
                    for i in range(0,self.k-self.index+1):
                        msg = next(self.msgs)
                    l = msg.message.ranges
                    self.index = self.k
                    
                else:
                    # previous index is greater than required index. It requires
                    # reading messages from the beginning as the container self.msgs
                    # only goes forward
                    self.msgs = self.bag.read_messages(topics=self.topic)
                    msg = next(self.msgs)
                    for i in range(1, self.k+1):
                        msg = next(self.msgs)
                        if abs(required_ts-self.l_ts[i]) < 0.1*abs(required_ts-self.l_ts[i-1]) or \
                            abs(required_ts-self.l_ts[i]) < NOMINAL_LIDAR_RATE*self.second_unit or \
                            i == self.k:
                            l = msg.message.ranges
                            self.index = i
                            break
            
            # When lidar readings come from txt files, they may be in mm units
            # rather than in meter (case of rosbag files). self.factor handles
            # these two situations to keep coordinates x, y in meters
            if np.mean(l) > 30:
                factor = 1000
            else:
                factor = 1
            self.factor=factor

            init_idx = 0
            if l[0] > 100*np.mean(l):
                init_idx = 1

            for i in range(init_idx, len(l)):
                if l[i] > self.lidar_min_dist_m*self.factor and l[i] < self.lidar_max_dist_m*self.factor and \
                    i >= self.lidar_index_begin and i <= self.lidar_index_end :
                    x.append(l[i]/self.factor*math.cos(self.angle[i] + self.rotate_degrees*math.pi/180 + self.angle_offset[self.k]))
                    y.append(l[i]/self.factor*math.sin(self.angle[i] + self.rotate_degrees*math.pi/180 + self.angle_offset[self.k]))
            self.pl.set_ydata(y)
            self.pl.set_xdata(x) 
            self.pl.set_visible(plot_visible) 
            if title_str == '':
                th = 'LiDAR xy plot [m] Scan ts: ' + str(self.l_ts[self.k]) + ' ms\n' + \
                    f3(0.001*(self.l_ts[self.k]-self.l_ts[0])) + '/' + \
                    f3(0.001*(self.l_ts[len(self.l_ts)-1]-self.l_ts[0])) + 's'
            else:
                th = title_str
            self.ax.set_title(th, fontdict={'fontsize':self.title_fontsize})
            
        def crossTimestampCheck(self, required_ts=0, add_str='', always_print=False ):
            if abs(required_ts-self.l_ts[self.index]) > NOMINAL_LIDAR_RATE or \
                always_print:
                print(add_str)
                print('diff[i-1]:',abs(required_ts-self.l_ts[self.index-1]))
                print('diff[i]:',abs(required_ts-self.l_ts[self.index]))
                print('diff[i+1]:',abs(required_ts-self.l_ts[self.index+1]))
        '''  

        def updateLine(pl, ax, slope, intercept, plot_visible=True):
            
            bottom, top = ax.get_ylim()
            y = np.array([bottom, top])
            x = slope*y+intercept
            pl.set_ydata(y)
            pl.set_xdata(x)
            pl.set_visible(plot_visible) 
        '''      


    class Line:
        __slots__ = ['fig', 'idx', 'plot_visible', 'ax', 'pl', 'rotate_rad', 'k', 'timestamp', 'slope', 'intercept_x', 'intercept']
        def __init__(self, fig=None, rect=[], slope=[0], intercept=[0], intercept_x=[0], timestamp=[], rotate_rad=0, idx = 0, alpha=1, color='k', marker='.', linestyle='--', linewidth=1, _ax=None, plot_visible=True):
            self.fig = fig
            self.slope = slope
            self.intercept = intercept
            self.intercept_x = intercept_x
            self.rotate_rad = rotate_rad
            self.timestamp = timestamp
            self.idx = idx
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
            else:
                self.ax = _ax
            bottom, top = self.ax.get_ylim()
            _y = np.array([bottom, top])


            if len(intercept_x) > 1: #vertical line
                _x = np.array([intercept_x[idx], intercept_x[idx]])
            else:                
                _x = slope[idx]*_y+intercept[idx]
            if self.rotate_rad:
                x = math.cos(self.rotate_rad)*_x - math.sin(self.rotate_rad)*_y
                y = math.sin(self.rotate_rad)*_x + math.cos(self.rotate_rad)*_y
            else:
                x = _x
                y = _y
            self.pl, = self.ax.plot(x, y, color=color, marker=marker,\
                        linestyle=linestyle, linewidth=linewidth, \
                        visible=plot_visible, alpha=alpha)
        def update(self, k= 1, slope=[], intercept=[], ts_to_find=None, title_str='', rotate_rad=None, title_fontsize=8, plot_visible=True):
            if len(slope) > 0:
                self.slope = slope
            if len(intercept) > 0:
                self.intercept = intercept
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
                print('k', k, 'ts_to_find', ts_to_find, 'x')
            self.idx = k
            bottom, top = self.ax.get_ylim()
            _y = np.array([bottom, top])
            if len(self.intercept_x) > 1: #vertical line
                _x = [self.intercept_x[k], self.intercept_x[k]]
            else:
                _x = self.slope[k]*_y+self.intercept[k]
        
            if rotate_rad is not None:
                self.rotate_rad = rotate_rad

            if self.rotate_rad:
                x = math.cos(self.rotate_rad)*_x - math.sin(self.rotate_rad)*_y
                y = math.sin(self.rotate_rad)*_x + math.cos(self.rotate_rad)*_y
            else:
                x = _x
                y = _y
            self.pl.set_ydata(y)
            self.pl.set_xdata(x)
            self.pl.set_visible(plot_visible) 

    class Line2:
        __slots__ = ['fig', 'idx', 'plot_visible', 'ax', 'pl', 'rotate_rad', 'k', 'timestamp', 'slope', 'intercept_x', 'intercept']
        def __init__(self, fig=None, rect=[], slope=[0], intercept=[0], intercept_x=[0], timestamp=[], rotate_rad=0, idx = 0, alpha=1, color='k', marker='.', linestyle='--', linewidth=1, _ax=None, plot_visible=True):
            self.fig = fig
            self.slope = slope
            self.intercept = intercept
            self.intercept_x = intercept_x
            self.rotate_rad = rotate_rad

            self.timestamp = timestamp
            self.idx = idx
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
            else:
                self.ax = _ax
            bottom, top = self.ax.get_ylim()
            left, right = self.ax.get_xlim()
            _x = np.array([left, right])


            if len(intercept_x) > 1: #vertical line
                _y = np.array([intercept_x[idx], intercept_x[idx]])
            else:                
                _y = slope[idx]*_x+intercept[idx]
            if self.rotate_rad:
                print('Line2 rotate_rad ' + str(self.rotate_rad))
                x = math.cos(self.rotate_rad)*_x - math.sin(self.rotate_rad)*_y
                y = math.sin(self.rotate_rad)*_x + math.cos(self.rotate_rad)*_y
            else:
                x = _x
                y = _y
            self.pl, = self.ax.plot(x, y, color=color, marker=marker,\
                        linestyle=linestyle, linewidth=linewidth, \
                        visible=plot_visible, alpha=alpha)
        def update(self, k= 1, slope=[], intercept=[], ts_to_find=None, title_str='', title_fontsize=8, plot_visible=True):
            if len(slope) > 0:
                self.slope = slope
            if len(intercept) > 0:
                self.intercept = intercept
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
                print('k', k, 'ts_to_find', ts_to_find, 'x')
            self.idx = k
            #bottom, top = self.ax.get_ylim()
            #_y = np.array([bottom, top])
            left, right = self.ax.get_xlim()
            _x = np.array([left, right])
            if len(self.intercept_x) > 1: #vertical line
                _y = [self.intercept_x[k], self.intercept_x[k]]
            else:
                _y = self.slope[k]*_x+self.intercept[k]

            if self.rotate_rad:
                x = math.cos(self.rotate_rad)*_x - math.sin(self.rotate_rad)*_y
                y = math.sin(self.rotate_rad)*_x + math.cos(self.rotate_rad)*_y
            else:
                x = _x
                y = _y
            self.pl.set_ydata(y)
            self.pl.set_xdata(x)
            self.pl.set_visible(plot_visible) 
     

    class Polar:
        def __init__(self, fig=None, rect=[], angle_rad=[], ray=[], marker='.', _ax=None, xlim=True, theta_min=0,theta_max=180, r_max=-1):
            self.fig = fig
            self.ray = ray

            if _ax == None:
                self.ax = self.fig.add_axes(rect, polar=True)
            else:
                self.ax = _ax
            if r_max == -1:
                r_max = np.nanmax(ray)
            self.pl, = self.ax.plot(angle_rad,ray,marker,ms=0.5)
            self.ax.set_thetamin(theta_min)
            self.ax.set_thetamax(theta_max)
            self.ax.set_rmax(r_max)
        def update(self, ray=[]):
            self.pl.set_ydata(ray)

    class XY_line:
        __slots__ = ['fig', 'idx', 'plot_visible', 'ax', 'pl', 'timestamp', 'x0', 'x1', 'y0', 'y1', 'title_fontsize']
        def __init__(self, fig=None, rect=[],  x0=[], y0=[], \
                     x1=[], y1=[], rotate_rad=0, timestamp=[], idx = 0, alpha=1,\
                     marker='x', marker_color='b', marker_size=1.4, \
                     linecolor='b', linewidth=1, linestyle='--',\
                     _xticks=True, _yticks=True, plot_visible=True,\
                     _ax=None, xlim=True, title_str='', title_fontsize=8, label=''):
            if rotate_rad != 0:
                x0, y0 = rotate(x0, y0, rotate_rad)      
                x1, y1 = rotate(x1, y1, rotate_rad)      
            self.x0 = x0
            self.y0 = y0
            self.x1 = x1
            self.y1 = y1
            self.timestamp=[]
            self.fig = fig
            self.idx = idx
            self.title_fontsize=title_fontsize
            
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
            else:
                self.ax = _ax

            self.pl, = self.ax.plot([x0[idx],x1[idx]], [y0[idx],y1[idx]], \
                                 marker=marker, ms=marker_size, alpha=alpha,\
                                 markeredgecolor=marker_color, \
                                 markerfacecolor="None",\
                                 linewidth=linewidth, color=linecolor,\
                                 linestyle=linestyle, visible=plot_visible, label=label)
            
            if not _xticks:
                self.ax.xticks([]) 
            if not _yticks:
                self.ax.yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize})                
        def update(self, k=1, ts_to_find=None, rotate_rad = None, plot_visible=True, title_str=''):
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]  
                print('k', k, 'ts_to_find', ts_to_find, 'x', self.x0[k])

            if rotate_rad is not None:
                _x0, _y0 = rotate(self.x0[k], self.y0[k], rotate_rad)      
                _x1, _y1 = rotate(self.x1[k], self.y1[k], rotate_rad)      
            else:
                _x0 = self.x0[k]
                _y0 = self.y0[k]
                _x1 = self.x1[k]
                _y1 = self.y1[k]
            self.idx = k        
            self.pl.set_ydata([_y0, _y1])
            self.pl.set_xdata([_x0, _x1])
            self.pl.set_visible(plot_visible)
            if title_str != '':
                self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize}) 
        def updateValues(self, x0, y0, x1, y1):
            self.x0 = x0
            self.y0 = y0
            self.x1 = x1
            self.y1 = y1
                   
    class XY_full: #supply the required (x,y) for each update/ no timestamp suport
        __slots__ = ['fig', 'idx', 'plot_visible', 'ax', 'pl', 'timestamp', 'x', 'y', 'windows_size', 'main_ax', 'pl_cur', 'title_str', 'title_fontsize']
        def __init__(self, fig=None, rect=[],  x=[], y=[], idx=0, rotate_rad=0.0, \
                     marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5, \
                     linewidth=1, focus=False, windows_size=10,\
                     _xticks=True, _yticks=True, plot_visible=True,\
                     _ax=None, xlim=True, title_str='', title_fontsize=8, label=''):
            self.x = x
            self.y = y
            self.fig = fig
            self.idx = idx
            self.title_fontsize = title_fontsize
            self.windows_size = windows_size
            left = np.nanmin(x) if len(x) > 0 else 1
            right = np.nanmax(x) if len(x) > 0 else 1 
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
                self.main_ax = True
            else:
                self.ax = _ax
                self.main_ax = False
                left, right = self.ax.get_xlim()
            if(rotate_rad != 0.0):
                x,y = rotate(x,y,rotate_rad)
            #print('marker_color: ' + str(marker_color) + ' linewidth: ' + str(linewidth) + ' len x: ' + str(len(x)) + ' y: ' + str(len(y))+ ' idx x: ' + str(x[idx]) + ' y: ' + str(y[idx]))
            
            self.pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                  markeredgecolor=marker_color, \
                                  color=marker_color, linewidth=linewidth, label=label)    
            self.pl_cur, = self.ax.plot(x[idx],y[idx],marker=update_marker,markeredgecolor=update_color,\
                           ms=update_marker_size, linewidth=linewidth, visible=plot_visible)  
            self.ax.set_xlim(left,right)                     
            
            if xlim:
                if left > np.nanmin(x):
                    left = np.nanmin(x)
                if right < np.nanmax(x):
                    right = np.nanmax(x)
                self.ax.set_xlim(left,right)
            if focus:     
                self.adjustLim(idx, windows_size)  
            if not _xticks:
                self.ax.set_xticks([]) 
            if not _yticks:
                self.ax.set_yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize})

        def adjustLim(self, idx=0, windows_size=10):
            idx_before = int(idx - 0.5*windows_size)
            idx_after = int(idx + 0.5*windows_size)
            if idx_before < 0:
                idx_before = 0
            if idx_after >= len(self.x):
                idx_after = len(self.x) - 1
            #print('adjust')
            #if math.isnan(self.x[idx_before]*self.x[idx_after]*self.y[idx_before]*self.y[idx_after]) is False : 
            left_limit, right_limit = get_safe_x_lims(left=self.x[idx_before], right = self.x[idx_after])
            top_limit, bottom_limit = get_safe_y_lims(self.y[idx_before:idx_after], self.y[idx_before:idx_after])
            #print('adjust isnan')

            if self.main_ax is False:
                _bottom, _top = self.ax.get_ylim()
                _left, _right = self.ax.get_xlim()
                #print('left', left_limit, '_left', _left, 'right', right_limit, '_right', _right, 'top', top_limit, '_top', _top, 'bottom', bottom_limit, '_bottom', _bottom)
                if top_limit < _top or math.isnan(top_limit):
                    top_limit = _top
                if bottom_limit > _bottom or math.isnan(bottom_limit):
                    bottom_limit = _bottom
                #if left_limit > _left:
                left_limit = _left
                #if right_limit < _right:
                right_limit = _right
            if math.isnan(left_limit*right_limit*top_limit*bottom_limit) is False :  
                self.ax.set_xlim([left_limit, right_limit])
                self.ax.set_ylim([bottom_limit, top_limit])

        def update(self, x=[], y=[], k=0, rotate_rad=0.0, focus=False, title_str=''):
            if focus:  
                self.adjustLim(k, self.windows_size) 

            if(rotate_rad != 0.0):
                x,y = rotate(x,y,rotate_rad)
            self.pl.set_ydata(y)
            self.pl.set_xdata(x)
            self.pl_cur.set_xdata(x[k])
            self.pl_cur.set_ydata(y[k])  


            if title_str != '':
                self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize}) 
            
    class XY_point:
        __slots__ = ['fig', 'idx', 'plot_visible', 'ax', 'pl', 'timestamp', 'x', 'y', 'windows_size', 'main_ax', 'pl_cur', '_pl', 'title_str', 'title_fontsize']

        def __init__(self, fig=None, rect=[],  x=[], y=[], timestamp=[], idx=0,\
                     marker='o', marker_color='b', marker_size=1, \
                     update_marker='x', update_color='r', update_marker_size=5, \
                     linewidth=1, focus=False, windows_size=10,\
                     _xticks=True, _yticks=True, plot_visible=True,\
                     _ax=None, xlim=True,title_str='', title_fontsize=8, label=''):

            if type(y) == 'pandas.core.series.Series' and y.dtype != 'float' and y.dtype != 'int':
                try:
                    for i in y.keys():
                        y[i] = float(y[i])
                except:
                    y = [float(i) for i in y]
            if update_color==None:
                update_color=marker_color
            if len(x) == 0:
                x = [0]
                y = [0]
            self.x = x
            self.y = y
            self.timestamp = np.array(timestamp)
            self.title_fontsize = title_fontsize
            self.idx = idx
            self.fig = fig
            left, right = get_safe_x_lims(x, x)
            
            if _ax == None:
                self.ax = self.fig.add_axes(rect)
                self.main_ax = True
                self._pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                     markeredgecolor=marker_color, \
                                     color=marker_color, linewidth=linewidth, visible=plot_visible, label=label)
            else:
                self.ax = _ax
                self.main_ax = False
                left, right = self.ax.get_xlim()
                self._pl, = self.ax.plot(x, y, marker=marker, ms=marker_size,\
                                     markeredgecolor=marker_color, \
                                     color=marker_color, linewidth=linewidth, visible=plot_visible, label=label)

                self.ax.set_xlim(left,right)
                
            self.pl, = self.ax.plot(x[idx],y[idx],marker=update_marker,markeredgecolor=update_color,\
                           ms=update_marker_size, linewidth=linewidth, visible=plot_visible)
            
            if xlim:
                if left > np.nanmin(x):
                    left = np.nanmin(x)
                if right < np.nanmax(x):
                    right = np.nanmax(x)
                self.ax.set_xlim(left,right)
            if focus:     
                self.adjustLim(idx, windows_size)           
            if not _xticks:
                self.ax.set_xticks([]) 
            if not _yticks:
                self.ax.set_yticks([]) 
            if title_str != '':
                self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize})
                
        def adjustLim(self, idx=0, windows_size=10):
            idx_before = int(idx - 0.5*windows_size)
            idx_after = int(idx + 0.5*windows_size)
            if idx_before < 0:
                idx_before = 0
            if idx_after >= len(self.x):
                idx_after = len(self.x) - 1

            idx_after = len(self.x) - 1 
            left_limit, right_limit = get_safe_x_lims(self.x[idx_before], self.x[idx_after])
            top_limit, bottom_limit = get_safe_x_lims(self.y[idx_before:idx_after], self.y[idx_before:idx_after])

            #print('adjust isnan')

            if self.main_ax is False:
                _bottom, _top = self.ax.get_ylim()
                _left, _right = self.ax.get_xlim()
                #print('left', left_limit, '_left', _left, 'right', right_limit, '_right', _right, 'top', top_limit, '_top', _top, 'bottom', bottom_limit, '_bottom', _bottom)
                if top_limit < _top or math.isnan(top_limit):
                    top_limit = _top
                if bottom_limit > _bottom or math.isnan(bottom_limit):
                    bottom_limit = _bottom
                #if left_limit > _left:
                left_limit = _left
                #if right_limit < _right:
                right_limit = _right
            if math.isnan(left_limit*right_limit*top_limit*bottom_limit) is False :  
                self.ax.set_xlim([left_limit, right_limit])
                self.ax.set_ylim([bottom_limit, top_limit])

        def update(self, k=1, x=None, y=None, ts_to_find=None, focus=False, windows_size=10, plot_visible=True, title_str=None, auto_title_str=False):
            
            if ts_to_find is not None:
                index_ts_greater_than_required = np.where(self.timestamp>=ts_to_find)[0] 
                if len(index_ts_greater_than_required) > 0 :                        
                    k = index_ts_greater_than_required[0]
            self.idx = k
            if x is not None:
                self.x = x
            if y is not None:
                self.y = y
            if not np.isnan(self.y[k]):
                self.pl.set_ydata(self.y[k])
            if not np.isnan(self.x[k]):
                self.pl.set_xdata(self.x[k]) 
            self.pl.set_visible(plot_visible)
            self._pl.set_visible(plot_visible)
            if focus:  
                self.adjustLim(k, windows_size)   
            if title_str != None:
                if title_str != '':
                    if auto_title_str:
                        self.ax.set_title(title_str + ' ' + f3(self.y[k]) + ' max ' + f3(np.nanmax(self.y)), fontdict={'fontsize':self.title_fontsize}) 
                    else:
                        self.ax.set_title(title_str, fontdict={'fontsize':self.title_fontsize}) 
                else:
                    self.ax.set_title(f3(self.y[k]), fontdict={'fontsize':self.title_fontsize})

    def initWidgets(self, slider_size=1, width=1):
        w0 = 0.05
        dw = width - 2*w0
        btn_dw = 0.5*(dw-0.02)



        self.axcolor = 'lightgoldenrodyellow'
        self.axscan = plt.axes([w0, 0.06, dw, 0.03], facecolor=self.axcolor)
        self.ax_btn_next = plt.axes([w0+btn_dw+0.02, 0.02, btn_dw, 0.03], facecolor=self.axcolor)
        self.ax_btn_prev = plt.axes([w0, 0.02, btn_dw, 0.03], facecolor=self.axcolor)
        self.slider_scan = Slider(self.axscan, '', 1, slider_size, valinit = 0, valstep=1)
        self.button_next = Button(self.ax_btn_next,'>', color=self.axcolor, hovercolor='0.975')    
        self.button_prev = Button(self.ax_btn_prev,'<', color=self.axcolor, hovercolor='0.975')
        self.button_next.on_clicked(self.next_scan)  
        self.button_prev.on_clicked(self.prev_scan)          
        self.slider_scan.on_changed(self.update)   
        self.n_scans_text= self.fig.text(0.01,0.005, 'step for <previous> <next> scan: ' + str(1))
    def press(self,event):      
        if event.key == 's':
            self.break_for = True  
        elif event.key == 'right':
            if DEBUG_WIDGETS:
                print('###################### next key')
            self.slider_scan.set_val(self.slider_scan.val + 1)
            #self.update()            
        elif event.key == 'left':
            if DEBUG_WIDGETS:
                print('###################### key')
            self.slider_scan.set_val(self.slider_scan.val - 1)
            #self.update()        
        self.fig.canvas.draw()     

    def next_scan(self,event):
        if DEBUG_WIDGETS:
            print('###################### next button')
        a = self.slider_scan.val + 1
        self.slider_scan.set_val(a)
      
    def prev_scan(self,event):        
        if DEBUG_WIDGETS:
            print('###################### prev button')
        a = self.slider_scan.val - 1
        self.slider_scan.set_val(a)
         
    def update(self, val):
        if DEBUG_WIDGETS:
            print('###################### slider')
        self.plots(int(self.slider_scan.val))
        self.fig.canvas.draw_idle()  
#import cv2
#from perception_utils import *
#from rosbag_utils import *
#from aux2plot import *