#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 09:40:11 2020

@author: dell
"""

class Display:
    def __init__(self):
        self.LEFT = 0
        self.RIGHT = 1
        self.width = [0]*2
        self.height = [0]*2
        self.left = [0]*2
        self.top = [0]*2
    
    def set(self, num, width, height, left_offset=0, top_offset=0):
        if num == 0 or num == 1:
            self.width[num] = width
            self.height[num] = height
            print(str(num) + ' width ' + str(self.width[num]))
            self.top[num] = top_offset
            self.left[num] = left_offset
            if num == 1:
                self.left[num] += self.width[self.LEFT]