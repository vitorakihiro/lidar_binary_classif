#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 07:37:34 2020

@author: dell
"""

import bisect, sys, time, yaml, math

import file_handling_utils
import handle_vars
import import_data_class
import cam_utils
#import utils
import numpy as np

def f3(val):
    if float(val).is_integer():
        return '{:,}'.format(int(val))    
    return '{:,.3f}'.format(val)

class HandleLogs:
    class Log:
        __slots__ = ['df', 'ts', 'idx', 'ts_label_to_sync']
        def __init__(self, idx=0, ts_label_to_sync='timestamp'):
            self.idx = idx
            self.ts_label_to_sync = ts_label_to_sync
        def get(self,field):
            return self.df[field][self.idx]
        def set_ts_label_to_sync(self, label='timestamp'):
            self.ts_label_to_sync = label
            self.ts = self.df[label]
        def sync(self, ts_to_match):
            self.idx = bisect.bisect_left(self.ts, ts_to_match, hi=len(self.ts)-1)
            print('{0: <{width}}'.format(self.name, width=20) + '\tlen ' + str(len(self.ts)) + '\tcur idx ' + '{0: <{width}}'.format(self.idx, width=10) + 'matched_ts ' + '{0: <{width}}'.format(self.ts[self.idx], width=10) + '\tdiff: ' + str(ts_to_match-self.ts[self.idx]))
            print('\ttimestamp range ' + f3(self.ts.iloc[0]) + ' to ' + f3(self.ts.iloc[-1]))
    class SystemLog(Log):
        def __init__(self, log, exp, idx=0):
            self.df = []
            if exp in list(log.datalog):
                self.df = log.datalog[exp]
                self.ts = list(self.df)
                self.idx = idx

    class Lidar:
        __slots__ = ['df', 'ts', 'idx', 'rotate_angle', 'angle']
        def __init__(self, log, exp, rotate_angle, idx=0):
            self.df = log.lidar[exp]
            self.ts = list(self.df)
            self.rotate_angle = rotate_angle
            self.angle = np.linspace(-3*math.pi/4+rotate_angle, 3*math.pi/4+rotate_angle, 1081, endpoint = False)
            self.idx = idx
        def getCurrentTimestamp(self):
            return self.ts[self.idx]
        def getXY(self, angle_offset=0, idx=0):
            if idx > len(self.ts):
                idx = len(self.ts)-1
            self.idx=idx
            l = self.df[self.ts[idx]]
            x=[]
            y=[]
            for i in range(0,len(l)):
                x.append(0.001*l[i]*math.cos(self.angle[i]+ angle_offset) )
                y.append(0.001*l[i]*math.sin(self.angle[i]+ angle_offset) )
            return x,y

        def __sizeof__(self):
            return object.__sizeof__(self) + sys.getsizeof(self.df) + sys.getsizeof(self.ts) + sys.getsizeof(self.rotate_angle) + sys.getsizeof(self.angle) + sys.getsizeof(self.idx)
    class Nav(Log):
        __slots__ = ['name']
        def __init__(self, log, exp, name='', idx=0, ts_label_to_sync='timestamp'):
            self.df=[]
            self.idx = idx
            self.name = name
            self.ts_label_to_sync = ts_label_to_sync
            if exp in list(log.nav):
                self.df = log.nav[exp]
                self.ts = self.df[ts_label_to_sync]

    class PerceptionLog(Log):
        __slots__ = ['rotate_angle', 'name', 'ts', 'idx', 'df']
        def __init__(self, log, exp, rotate_angle, name='', idx=0, ts_label_to_sync='timestamp'):
            self.df = log.pTS[exp]
            self.ts_label_to_sync = ts_label_to_sync
            self.ts = self.df[ts_label_to_sync]
            self.rotate_angle = rotate_angle
            self.idx=idx
            self.name = name
            self.fix()

        def __sizeof__(self):
            return object.__sizeof__(self) + sys.getsizeof(self.df) + sys.getsizeof(self.ts) + sys.getsizeof(self.rotate_angle) + sys.getsizeof(self.idx)
        def fix(self):
            nan_vec = [0]+[np.nan]*(len(self.ts)-1)
            
            if 'angular_vel_kf2' not in list(self.df):
                self.df['angular_vel_kf2'] = nan_vec
            if 'Automatic' not in list(self.df):
                self.df['Automatic'] = nan_vec
            if 'BackRight speed' not in list(self.df):
                self.df['BackRight speed'] = nan_vec
            if 'BackLeft speed' not in list(self.df):
                self.df['BackLeft speed'] = nan_vec
            if 'dead_count' not in list(self.df):
                self.df['dead_count'] = nan_vec
            if 'desired_throttle' not in list(self.df):
                self.df['desired_throttle'] = nan_vec
            if 'desired_yaw' not in list(self.df):
                self.df['desired_yaw'] = nan_vec
            if 'ekf_input_heading' not in list(self.df):
                self.df['ekf_input_heading'] = nan_vec
            if 'ekf_input_wz' not in list(self.df):
                self.df['ekf_input_wz'] = nan_vec
            if 'ekf_input_vx' not in list(self.df):
                self.df['ekf_input_vx'] = nan_vec
            if 'ekf_x' not in list(self.df):
                self.df['ekf_x'] = nan_vec
            if 'ekf_y' not in list(self.df):
                self.df['ekf_y'] = nan_vec
            if 'filtered_heading' not in list(self.df):
                self.df['filtered_heading'] = nan_vec
            if 'Final_Vx' not in list(self.df):
                self.df['Final_Vx'] = nan_vec
            if 'FinalWz' not in list(self.df):
                self.df['FinalWz'] = nan_vec
            if 'FrontRight speed' not in list(self.df):
                self.df['FrontRight speed'] = nan_vec
            if 'FrontLeft speed' not in list(self.df):
                self.df['FrontLeft speed'] = nan_vec
            if 'heading_kf2' not in list(self.df):
                self.df['heading_kf2'] = nan_vec
            if 'hist_index_l' not in list(self.df):
                self.df['hist_index_l'] = nan_vec
            if 'hist_index_r' not in list(self.df):
                self.df['hist_index_r'] = nan_vec
            if 'hist_peak_l' not in list(self.df):
                self.df['hist_peak_l'] = nan_vec
            if 'hist_peak_r' not in list(self.df):
                self.df['hist_peak_r'] = nan_vec
            if 'inrow_box_used_heading' not in list(self.df):
                self.df['inrow_box_used_heading'] = nan_vec
            if 'is_imu_available' not in list(self.df):
                self.df['is_imu_available'] = nan_vec
            if 'LBEspeed' not in list(self.df):
                self.df['LBEspeed'] = nan_vec
            if 'LFEspeed' not in list(self.df):
                self.df['LFEspeed'] = nan_vec
            if 'lidar_tags_ts_ms' not in list(self.df):
                self.df['lidar_tags_ts_ms'] = nan_vec
            if 'line_pts_l' not in list(self.df):
                self.df['line_pts_l'] = nan_vec
            if 'line_pts_r' not in list(self.df):
                self.df['line_pts_r'] = nan_vec
            if 'Linear_vel_kf2' not in list(self.df):
                self.df['Linear_vel_kf2'] = nan_vec        
            if 'n_executions_from_reset' not in list(self.df):
                self.df['n_executions_from_reset'] = nan_vec
            if 'pearson_l' not in list(self.df):
                self.df['pearson_l'] = nan_vec
            if 'pearson_r' not in list(self.df):
                self.df['pearson_r'] = nan_vec
            if 'r_squared_l' not in list(self.df):
                self.df['r_squared_l'] = nan_vec
            if 'r_squared_r' not in list(self.df):
                self.df['r_squared_r'] = nan_vec
            if 'RBEspeed' not in list(self.df):
                self.df['RBEspeed'] = nan_vec
            if 'RFEspeed' not in list(self.df):
                self.df['RFEspeed'] = nan_vec
            if 'signal' not in list(self.df):
                self.df['signal'] = nan_vec
            if 'split_size_l' not in list(self.df):
                self.df['split_size_l'] = nan_vec
            if 'split_size_r' not in list(self.df):
                self.df['split_size_r'] = nan_vec
            if 'split_x0_l' not in list(self.df):
                self.df['split_x0_l'] = nan_vec
            if 'split_x0_r' not in list(self.df):
                self.df['split_x0_r'] = nan_vec
            if 'split_y0_l' not in list(self.df):
                self.df['split_y0_l'] = nan_vec
            if 'split_y0_r' not in list(self.df):
                self.df['split_y0_r'] = nan_vec
            if 'split_x1_l' not in list(self.df):
                self.df['split_x1_l'] = nan_vec
            if 'split_x1_r' not in list(self.df):
                self.df['split_x1_r'] = nan_vec
            if 'split_y1_l' not in list(self.df):
                self.df['split_y1_l'] = nan_vec
            if 'split_y1_r' not in list(self.df):
                self.df['split_y1_r'] = nan_vec
            if 'state' not in list(self.df):
                self.df['state'] = nan_vec
            if 'sum_orthogonal_dist_l' not in list(self.df):
                self.df['sum_orthogonal_dist_l'] = nan_vec
            if 'sum_orthogonal_dist_r' not in list(self.df):
                self.df['sum_orthogonal_dist_r'] = nan_vec
            if 'tablet_throttle' not in list(self.df):
                self.df['tablet_throttle'] = nan_vec
            if 'tablet_yaw_rate' not in list(self.df):
                self.df['tablet_yaw_rate'] = nan_vec
            if 'target_heading' not in list(self.df):
                self.df['target_heading'] = nan_vec
            if 't_runHistogram_afterFcnHistogram_l' not in list(self.df):
                self.df['t_runHistogram_afterFcnHistogram_l'] = nan_vec
            if 't_runHistogram_afterIndexMaxDouble_l' not in list(self.df):
                self.df['t_runHistogram_afterIndexMaxDouble_l'] = nan_vec
            if 't_runHistogram_afterFcnHistogram_r' not in list(self.df):
                self.df['t_runHistogram_afterFcnHistogram_r'] = nan_vec
            if 't_runHistogram_afterIndexMaxDouble_r' not in list(self.df):
                self.df['t_runHistogram_afterIndexMaxDouble_r'] = nan_vec
            if 't_runPerception_us' not in list(self.df):
                self.df['t_runPerception_us'] = nan_vec
            if 'y_readings_outer_lim_l' not in list(self.df):
                self.df['y_readings_outer_lim_l'] = nan_vec
            if 'y_readings_outer_lim_r' not in list(self.df):
                self.df['y_readings_outer_lim_r'] = nan_vec
            if 'y_readings_inner_lim_l' not in list(self.df):
                self.df['y_readings_inner_lim_l'] = nan_vec
            if 'y_readings_inner_lim_r' not in list(self.df):
                self.df['y_readings_inner_lim_r'] = nan_vec
            if 'wcf' not in list(self.df):
                self.df['wcf'] = nan_vec
            if 't_calc_ellapsedFromStart' not in list(self.df):
                self.df['t_calc_ellapsedFromStart'] = nan_vec
            if 't_calc_ellapsedAfterSplit' not in list(self.df):
                self.df['t_calc_ellapsedAfterSplit'] = nan_vec
            if 't_calc_ellapsedAfterHist' not in list(self.df):
                self.df['t_calc_ellapsedAfterHist'] = nan_vec
            if 't_calc_ellapsedAfterUpdateHist' not in list(self.df):
                self.df['t_calc_ellapsedAfterUpdateHist'] = nan_vec
            if 't_calc_ellapsedAfterLineEst' not in list(self.df):
                self.df['t_calc_ellapsedAfterLineEst'] = nan_vec
            if 't_calc_ellapsedAfterCheckEstimatedLaneWidth' not in list(self.df):
                self.df['t_calc_ellapsedAfterCheckEstimatedLaneWidth'] = nan_vec
            if 't_calc_ellapsedAfterGetLinePoints' not in list(self.df):
                self.df['t_calc_ellapsedAfterGetLinePoints'] = nan_vec
            if 't_calc_ellapsedTimeAfterHeuristics' not in list(self.df):
                self.df['t_calc_ellapsedTimeAfterHeuristics'] = nan_vec
            if 't_calc_ellapsedTimeAfterPlEkf' not in list(self.df):
                self.df['t_calc_ellapsedTimeAfterPlEkf'] = nan_vec
            if 't_calc_ellapsedTimeBetweenMeas' not in list(self.df):
                self.df['t_calc_ellapsedTimeBetweenMeas'] = nan_vec
            if 't_calc_ellapsedTimeAfterUpdate' not in list(self.df):
                self.df['t_calc_ellapsedTimeAfterUpdate'] = nan_vec
            if 't_process_lidar_us' not in list(self.df):
                self.df['t_process_lidar_us'] = nan_vec
            if 't_estimateLaneHeading_us' not in list(self.df):
                self.df['t_estimateLaneHeading_us'] = nan_vec
            if 't_estimateRemainingLane_us' not in list(self.df):
                self.df['t_estimateRemainingLane_us'] = nan_vec
            if 't_getBisect_us' not in list(self.df):
                self.df['t_getBisect_us'] = nan_vec
            if 't_setXYlidar_us' not in list(self.df):
                self.df['t_setXYlidar_us'] = nan_vec
            if 't_smoothCurve_us' not in list(self.df):
                self.df['t_smoothCurve_us'] = nan_vec
            if 't_updateEncoder_us' not in list(self.df):
                self.df['t_updateEncoder_us'] = nan_vec
            if 't_calculatePID_us' not in list(self.df):
                self.df['t_calculatePID_us'] = nan_vec
            if 't_stopRobotOutRow_us' not in list(self.df):
                self.df['t_stopRobotOutRow_us'] = nan_vec
            if 't_updateDesiredThrottleYaw_us' not in list(self.df):
                self.df['t_updateDesiredThrottleYaw_us'] = nan_vec
            if 't_setDriveCommands_us' not in list(self.df):
                self.df['t_setDriveCommands_us'] = nan_vec
            if 't_runOdometry_us' not in list(self.df):
                self.df['t_runOdometry_us'] = nan_vec
            if 't_firstIteration_us' not in list(self.df):
                self.df['t_firstIteration_us'] = nan_vec
            if 't_updateLidarReadings_us' not in list(self.df):
                self.df['t_updateLidarReadings_us'] = nan_vec
            if 't_updateLidarReadings_copy_us' not in list(self.df):
                self.df['t_updateLidarReadings_copy_us'] = nan_vec
            if 't_updateLidarReadings_mutex_us' not in list(self.df):
                self.df['t_updateLidarReadings_mutex_us'] = nan_vec
            if 't_updateLidarReadings_setLidarData_us' not in list(self.df):
                self.df['t_updateLidarReadings_setLidarData_us'] = nan_vec
            if 't_updateHasExitedRow_us' not in list(self.df):
                self.df['t_updateHasExitedRow_us'] = nan_vec
            if 't_switchPIDcontroller_us' not in list(self.df):
                self.df['t_switchPIDcontroller_us'] = nan_vec
            if 't_runPerception_us' not in list(self.df):
                self.df['t_runPerception_us'] = nan_vec
            if 't_setXYlidar_us' not in list(self.df):
                self.df['t_setXYlidar_us'] = nan_vec
            if 't_ekf_us' not in list(self.df):
                self.df['t_ekf_us'] = nan_vec
            if 't_ekf_us0' not in list(self.df):
                self.df['t_ekf_us0'] = nan_vec
            if 't_ekf_us1' not in list(self.df):
                self.df['t_ekf_us1'] = nan_vec
            if 't_ekf_us2' not in list(self.df):
                self.df['t_ekf_us2'] = nan_vec
            if 't_ekf_us3' not in list(self.df):
                self.df['t_ekf_us3'] = nan_vec
            if 't_ekf_update_us0' not in list(self.df):
                self.df['t_ekf_update_us0'] = nan_vec
            if 't_ekf_update_us1' not in list(self.df):
                self.df['t_ekf_update_us1'] = nan_vec
            if 't_ekf_update_us2' not in list(self.df):
                self.df['t_ekf_update_us2'] = nan_vec
            if 't_ekf_update_us3' not in list(self.df):
                self.df['t_ekf_update_us3'] = nan_vec
            if 't_ekf_update_us4' not in list(self.df):
                self.df['t_ekf_update_us4'] = nan_vec
            if 't_smoothCurve_us' not in list(self.df):
                self.df['t_smoothCurve_us'] = nan_vec
            elif np.mean(self.df['t_smoothCurve_us']) < 0:
                self.df['t_smoothCurve_us'] = nan_vec
            if 't_updateEncoder_us' not in list(self.df):
                self.df['t_updateEncoder_us'] = nan_vec
            elif np.mean(self.df['t_updateEncoder_us']) < 0:
                self.df['t_updateEncoder_us'] = nan_vec
            if 't_calculatePID_us' not in list(self.df):
                self.df['t_calculatePID_us'] = nan_vec
            elif np.mean(self.df['t_calculatePID_us']) < 0:
                self.df['t_calculatePID_us'] = nan_vec
            if 't_stopRobotOutRow_us' not in list(self.df):
                self.df['t_stopRobotOutRow_us'] = nan_vec
            elif np.mean(self.df['t_stopRobotOutRow_us']) < 0:
                self.df['t_stopRobotOutRow_us'] = nan_vec
            if 't_updateDesiredThrottleYaw_us' not in list(self.df):
                self.df['t_updateDesiredThrottleYaw_us'] = nan_vec
            elif np.mean(self.df['t_updateDesiredThrottleYaw_us']) < 0:
                self.df['t_updateDesiredThrottleYaw_us'] = nan_vec
            if 't_setDriveCommands_us' not in list(self.df):
                self.df['t_setDriveCommands_us'] = nan_vec
            elif np.mean(self.df['t_setDriveCommands_us']) < 0:
                self.df['t_setDriveCommands_us'] = nan_vec
            if 't_runOdometry_us' not in list(self.df):
                self.df['t_runOdometry_us'] = nan_vec
            elif np.mean(self.df['t_runOdometry_us']) < 0:
                self.df['t_runOdometry_us'] = nan_vec
            if 't_firstIteration_us' not in list(self.df):
                self.df['t_firstIteration_us'] = nan_vec
            elif np.mean(self.df['t_firstIteration_us']) < 0:
                self.df['t_firstIteration_us'] = nan_vec
            if 't_updateLidarReadings_us' not in list(self.df):
                self.df['t_updateLidarReadings_us'] = nan_vec
            elif np.mean(self.df['t_updateLidarReadings_us']) < 0:
                self.df['t_updateLidarReadings_us'] = nan_vec
            if 't_updateLidarReadings_copy_us' not in list(self.df):
                self.df['t_updateLidarReadings_copy_us'] = nan_vec
            elif np.mean(self.df['t_updateLidarReadings_copy_us']) < 0:
                self.df['t_updateLidarReadings_copy_us'] = nan_vec
            if 't_updateLidarReadings_mutex_us' not in list(self.df):
                self.df['t_updateLidarReadings_mutex_us'] = nan_vec
            elif np.mean(self.df['t_updateLidarReadings_mutex_us']) < 0:
                self.df['t_updateLidarReadings_mutex_us'] = nan_vec
            if 't_updateLidarReadings_setLidarData_us' not in list(self.df):
                self.df['t_updateLidarReadings_setLidarData_us'] = nan_vec
            elif np.mean(self.df['t_updateLidarReadings_setLidarData_us']) < 0:
                self.df['t_updateLidarReadings_setLidarData_us'] = nan_vec
            if 't_updateHasExitedRow_us' not in list(self.df):
                self.df['t_updateHasExitedRow_us'] = nan_vec
            elif np.mean(self.df['t_updateHasExitedRow_us']) < 0:
                self.df['t_updateHasExitedRow_us'] = nan_vec
            if 't_switchPIDcontroller_us' not in list(self.df):
                self.df['t_switchPIDcontroller_us'] = nan_vec
            elif np.mean(self.df['t_switchPIDcontroller_us']) < 0:
                self.df['t_switchPIDcontroller_us'] = nan_vec
            '''
            if 't_calc_ellapsedFromStart' in list(self.df):
                for i in range(0, len(self.df['t_calc_ellapsedTimeAfterUpdate'])):
                    self.df['t_calc_ellapsedTimeAfterUpdate'][i] -= self.df['t_calc_ellapsedTimeAfterPlEkf'][i].copy()
                    self.df['t_calc_ellapsedTimeAfterPlEkf'][i] -= self.df['t_calc_ellapsedTimeAfterHeuristics'][i].copy()
                    self.df['t_calc_ellapsedTimeAfterHeuristics'][i] -= self.df['t_calc_ellapsedAfterGetLinePoints'][i].copy()
                    self.df['t_calc_ellapsedAfterGetLinePoints'][i] -= self.df['t_calc_ellapsedAfterCheckEstimatedLaneWidth'][i].copy()
                    self.df['t_calc_ellapsedAfterCheckEstimatedLaneWidth'][i] -= self.df['t_calc_ellapsedAfterLineEst'][i].copy()
                    self.df['t_calc_ellapsedAfterLineEst'][i] -= self.df['t_calc_ellapsedAfterUpdateHist'][i].copy()
                    self.df['t_calc_ellapsedAfterUpdateHist'][i] -= self.df['t_calc_ellapsedAfterHist'][i].copy()
                    self.df['t_calc_ellapsedAfterHist'][i] -= self.df['t_calc_ellapsedAfterSplit'][i].copy()
                    self.df['t_calc_ellapsedAfterSplit'][i] -= self.df['t_calc_ellapsedFromStart'][i].copy()
                    '''
            #LEGACY
            if 'sl' in list(self.df):
                self.df['slope_l'] = self.df.sl
                self.df['slope_r'] = self.df.sr
                self.df['intercept_l'] = self.df.il
                self.df['intercept_r'] = self.df.ir

    def getAvailableTimestamp(self, log, exp):
        timestamp_available = False
        timestamp_earliest_begin = 0
        timestamp_aux = []
        if exp in list(log.lidar):
            if 'timestamp' in list(log.lidar[exp]):
                timestamp_available = True
                timestamp_earliest_begin = log.lidar[exp].timestamp[0]
                timestamp_aux = log.lidar[exp].timestamp
        if exp in list(log.pTS):
            if 'timestamp' in list(log.pTS[exp]):
                if (timestamp_available and log.pTS[exp].timestamp[0]<timestamp_earliest_begin) or not timestamp_available:
                    timestamp_earliest_begin = log.pTS[exp].timestamp[0]
                    timestamp_available = True
                    timestamp_aux = log.pTS[exp].timestamp
        if exp in list(log.nav):
            if 'timestamp' in list(log.nav):
                print('')



    def __init__(self, path_to_yaml, rotate_angle, exp_original_only=False, ts_label_to_sync='timestamp'):
        with open(path_to_yaml) as file:
            paths = yaml.load(file)
        self.exp_original_only = exp_original_only
        # loading logs
        logs_original, logs_ref, logs_0, self.exp_original, self.exp_ref, self.exp_0 = handle_vars.adjustVars(paths)
        #self.available_ts = self.getAvailableTimestamp(logs_original, self.exp_original)
        self.front_ts_cam = cam_utils.TScam(logs_original, self.exp_original)
        # lidar from original log
        print('exp_original ' + self.exp_original)
        self.lidar = self.Lidar(logs_original, self.exp_original, rotate_angle)
        self.nav = self.Nav(logs_original, self.exp_original, name='nav')
        self.system_log = self.SystemLog(logs_original, self.exp_original)

        # df perception logs
        self.p_original = self.PerceptionLog(logs_original, self.exp_original, rotate_angle, name='p_original', ts_label_to_sync=ts_label_to_sync)
        #self.p_original.set_ts_label_to_sync('lidar_ts_ms')
        if not exp_original_only:
            self.p_ref = self.PerceptionLog(logs_ref, self.exp_ref, rotate_angle, name='p_ref', ts_label_to_sync=ts_label_to_sync)
            self.p_0 = self.PerceptionLog(logs_0, self.exp_0, rotate_angle, name='p_0', ts_label_to_sync=ts_label_to_sync)
            #self.p_0.set_ts_label_to_sync('lidar_ts_ms')

            # nav files - probably only from original


    def __sizeof__(self):
        return object.__sizeof__(self) + sum(sys.getsizeof(v) for v in self.__dict__.values())
        
        #+ sys.getsizeof(self.available_ts) + sys.getsizeof(self.front_ts_cam) + sys.getsizeof(self.lidar) + sys.getsizeof(self.system_log) + sys.getsizeof(self.p_original) +  sys.getsizeof(self.p_ref) +  sys.getsizeof(self.p_0) + sys.getsizeof(self.nav)   

    def sync(self, lidar_idx=0):
        if lidar_idx != 0:
            self.lidar.idx = lidar_idx
        ts_to_match = self.lidar.getCurrentTimestamp()
        print('lidar len ' + str(len(self.lidar.ts)) + ' cur idx ' + str(self.lidar.idx) + ' ts_to_match ' + str(ts_to_match) )
        self.p_original.sync(ts_to_match)
        self.nav.sync(ts_to_match)
        if not self.exp_original_only:
            self.p_ref.sync(ts_to_match)
            self.p_0.sync(ts_to_match)