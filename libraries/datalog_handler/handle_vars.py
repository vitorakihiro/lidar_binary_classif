#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 20:10:40 2020

@author: dell
"""
import sys
import numpy as np

from import_data_class import *

nav_header = ['timestamp', 'gps_lat', 'gps_lon', 'gps_heading', 'gps_spd',
       'gps_spd_x', 'gps_spd_y', 'gps_accuracy', 'gps_x', 'gps_y',
       'accel_x', 'accel_y', 'accel_z', 'gyro_yaw', 'gyro_pitch',
       'gyro_roll', 'mag_x', 'mag_y', 'mag_z', 'ekf_x', 'ekf_y', 'ekf_z',
       'ekf_vel_x', 'ekf_vel_y', 'ekf_vel_z', 'ekf_roll', 'ekf_pitch',
       'ekf_yaw', 'gyro_bx', 'gyro_by', 'gyro_bz', 'accel_bx', 'accel_by',
       'accel_bz', 'corrected_yaw_speed', 'TurnRate_command',
       'Speed_command', 'TurnRatePercent_command',
       'ThrottlePercent_command', 'reference_speed', 'mpc_input_heading',
       'mpc_output_omega', 'mpc_output_heading_diff',
       'mpc_output_xtrack_error', 'mpc_output_solver_time',
       'mpc_pred_vals_x0', 'mpc_pred_vals_y0', 'mpc_pred_vals_x1',
       'mpc_pred_vals_y1', 'mpc_pred_vals_x2', 'mpc_pred_vals_y2',
       'mpc_pred_vals_x3', 'mpc_pred_vals_y3', 'mpc_pred_vals_x4',
       'mpc_pred_vals_y4', 'mpc_pred_vals_x5', 'mpc_pred_vals_y5',
       'mpc_pred_vals_x6', 'mpc_pred_vals_y6', 'mpc_pred_vals_x7',
       'mpc_pred_vals_y7', 'mpc_pred_vals_x8', 'mpc_pred_vals_y8',
       'in_wp_x0', 'in_wp_y0', 'in_wp_x1', 'in_wp_y1', 'in_wp_x2',
       'in_wp_y2', 'in_wp_x3', 'in_wp_y3', 'in_wp_x4', 'in_wp_y4',
       'in_wp_x5', 'in_wp_y5', 'in_wp_x6', 'in_wp_y6', 'in_wp_x7',
       'in_wp_y7', 'in_wp_x8', 'in_wp_y8', 'in_wp_x9', 'in_wp_y9',
       'in_wp_x10', 'in_wp_y10', 'in_wp_x11', 'in_wp_y11', 'in_wp_x12',
       'in_wp_y12', 'in_wp_x13', 'in_wp_y13', 'in_wp_x14', 'in_wp_y14',
       'in_wp_x15', 'in_wp_y15', 'in_wp_x16', 'in_wp_y16', 'in_wp_x17',
       'in_wp_y17', 'in_wp_x18', 'in_wp_y18', 'in_wp_x19', 'in_wp_y19']

def adjustPathNames(path):
    if path.rfind('/'):
        if path.rfind('/') == len(path) - 1:
            print('remove last / from path ' + str(path))
            sys.exit()
        data_dir = path[0:path.rfind('/')]
        path = path.split('/')[-1]
        return path, data_dir
    
def adjustVars(paths, import_lidar_ref=False, import_lidar_0=False):
    
    exp_original,data_dir_original = adjustPathNames(paths['exp_original_path'])
    log_original = ImportedData(data_dir_original, import_exp=exp_original, import_lidar=True)

    if exp_original in list(log_original.pTS):
        nan_p_original_vec = [0]+[np.nan]*(len(log_original.pTS[exp_original].timestamp)-1)

        if exp_original not in list(log_original.nav):
            data = {}
            for name in nav_header:
                data[name] = nan_p_original_vec
            log_original.nav[exp_original] = pd.DataFrame(data)            
            log_original.nav[exp_original]['up_time'] = pd.Series(list(log_original.pTS[exp_original].timestamp))
            #log_original.nav[exp_original].update(timestamp=pd.Series(list(log_original.pTS[exp_original].timestamp)))
        else:
            if 'timestamp' not in list(log_original.nav[exp_original]):
                aux = log_original.nav[exp_original]['up_time']
                for i in range(0, len(aux)):
                    aux[i] = 0.001*aux[i]
                log_original.nav[exp_original]['timestamp'] = aux


        if 'Automatic' not in list(log_original.pTS[exp_original]):
            log_original.pTS[exp_original]['Automatic'] = nan_p_original_vec
        if 'lidar_tags_ts_ms' not in list(log_original.pTS[exp_original]):
            log_original.pTS[exp_original]['lidar_tags_ts_ms'] = nan_p_original_vec
        if 'wcf' not in list(log_original.pTS[exp_original]):
            log_original.pTS[exp_original]['wcf'] = nan_p_original_vec 


    if 'exp_ref' in paths:
        exp_ref, data_dir_ref = adjustPathNames(paths['exp_ref'])

        if exp_ref != exp_original:
            log_ref = ImportedData(data_dir_ref, import_exp=exp_ref, import_lidar=import_lidar_ref)
        else:
            log_ref = log_original

        nan_p_ref_vec = [0]+[np.nan]*(len(log_ref.pTS[exp_ref].timestamp)-1)

        if 'Automatic' not in list(log_ref.pTS[exp_ref]):
            log_ref.pTS[exp_ref]['Automatic'] = nan_p_ref_vec
        if 'ekf_input_wz' not in list(log_ref.pTS[exp_ref]):
            log_ref.pTS[exp_ref]['ekf_input_wz'] = nan_p_ref_vec
            log_ref.pTS[exp_ref]['ekf_input_vx'] = nan_p_ref_vec
        if 'heading_kf2' not in list(log_ref.pTS[exp_ref]):
            log_ref.pTS[exp_ref]['heading_kf2'] = nan_p_ref_vec

        if 'tablet_throttle' not in list(log_ref.pTS[exp_ref]): 
            log_ref.pTS[exp_ref]['tablet_throttle'] = nan_p_ref_vec
            log_ref.pTS[exp_ref]['tablet_yaw_rate'] = nan_p_ref_vec
        if 'y_readings_outer_lim_l' not in list(log_ref.pTS[exp_ref]): 
            log_ref.pTS[exp_ref]['y_readings_outer_lim_l'] = nan_p_ref_vec 
            log_ref.pTS[exp_ref]['y_readings_outer_lim_r'] = nan_p_ref_vec 
            log_ref.pTS[exp_ref]['y_readings_inner_lim_l'] = nan_p_ref_vec 
            log_ref.pTS[exp_ref]['y_readings_inner_lim_r'] = nan_p_ref_vec
    else:
        log_ref = None
        exp_ref = None
        
    if 'exp' in paths:
        exp_0, data_dir_0 = adjustPathNames(paths['exp'])       

        if exp_0 != exp_original:
            log_0 = ImportedData(data_dir_0, import_exp=exp_0, import_lidar=import_lidar_0)
        else:
            log_0 = log_original
        nan_p_0_vec = [0]+[np.nan]*(len(log_0.pTS[exp_0].timestamp)-1)

        if 'ekf_input_wz' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['ekf_input_wz'] = nan_p_0_vec
            log_0.pTS[exp_0]['ekf_input_vx'] = nan_p_0_vec
        if 'filtered_heading' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['filtered_heading'] = nan_p_0_vec
        if 'Final_Vx' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['Final_Vx'] = nan_p_0_vec
        if 'inrow_box_used_heading' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['inrow_box_used_heading'] = nan_p_0_vec
        if 'lidar_tags_ts_ms' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['lidar_tags_ts_ms'] = nan_p_0_vec
        if 'line_pts_l' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['line_pts_l'] = nan_p_0_vec
            log_0.pTS[exp_0]['line_pts_r'] = nan_p_0_vec
        if 'signal' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['signal'] = nan_p_0_vec
        if 'state' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['state'] = nan_p_0_vec
        if 'sum_orthogonal_dist_l' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['sum_orthogonal_dist_l'] = nan_p_0_vec
            log_0.pTS[exp_0]['sum_orthogonal_dist_r'] = nan_p_0_vec
        if 'target_heading' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['target_heading'] = nan_p_0_vec
        if 'y_readings_outer_lim_l' not in list(log_0.pTS[exp_0]): 
            log_0.pTS[exp_0]['y_readings_outer_lim_l'] = nan_p_0_vec 
            log_0.pTS[exp_0]['y_readings_outer_lim_r'] = nan_p_0_vec 
            log_0.pTS[exp_0]['y_readings_inner_lim_l'] = nan_p_0_vec 
            log_0.pTS[exp_0]['y_readings_inner_lim_r'] = nan_p_0_vec 
        if 'wcf' not in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['wcf'] = nan_p_0_vec

        #LEGACY
        if 'sl' in list(log_0.pTS[exp_0]):
            log_0.pTS[exp_0]['slope_l'] = log_0.pTS[exp_0].sl
            log_0.pTS[exp_0]['slope_r'] = log_0.pTS[exp_0].sr
            log_0.pTS[exp_0]['intercept_l'] = log_0.pTS[exp_0].il
            log_0.pTS[exp_0]['intercept_r'] = log_0.pTS[exp_0].ir
    else:
        log_0 = None
        exp_0 = None





    return log_original, log_ref, log_0, exp_original, exp_ref, exp_0