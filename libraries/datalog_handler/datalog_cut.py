#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 09:55:46 2020

@author: dell
"""
import sys, os

def cut_file(path_to_collection, file_dir_name, file_name, cut_from_timestamp, cut_to_timestamp, custom_name='', n_header_lines2cp=0, timestamp_field=0):
    file2cp_path = os.path.join(path_to_collection, file_dir_name)
    file2cp_path = os.path.join(file2cp_path, file_name)
    if os.path.isfile(file2cp_path):
        cut_collection_name = 'cut_' + str(os.path.basename(path_to_collection)) + '_' + custom_name
        cut_collection_path = os.path.join(os.path.dirname(path_to_collection), cut_collection_name)
        cut_file_dir = os.path.join(cut_collection_path, file_dir_name)
        cut_file_path = os.path.join(cut_file_dir, file_name)

        if not os.path.exists(cut_collection_path):
            os.makedirs(cut_collection_path)
        else:
            print('Already created')
        if not os.path.exists(cut_file_dir):
            os.makedirs(cut_file_dir)
        else:
            print('Already created')

        f = open(file2cp_path)
        cut_file = open(str(cut_file_path), 'w')
        content = f.read().splitlines()
        for i in range(0, n_header_lines2cp):
            cut_file.write(content[i]+'\n')
        for i in range(n_header_lines2cp, len(content)):
            fields = content[i].split(",")
            if cut_from_timestamp < int(fields[timestamp_field]) < cut_to_timestamp:
                cut_file.write(content[i])
                cut_file.write('\n')   

        return str(cut_collection_path)
    else:
        print('Check cut_datalog. Trying to cut file: ' + str(file2cp_path))
        return ''


def cut(path_to_collection, cut_from_timestamp, cut_to_timestamp, custom_name='', add_all_paths=False):
    cut_file(path_to_collection, 'lidar', 'lidar_log', cut_from_timestamp, cut_to_timestamp, custom_name, 0)
    new_collection_path = cut_file(path_to_collection, 'lidar', 'perception_lidar_log', cut_from_timestamp, cut_to_timestamp, custom_name, 2, timestamp_field=1)


    #add to yaml file
    with open('paths.yaml', 'a') as f:
        
        f.write('\n#Added with datalog_cut.py')
        f.write('\ngt_path: \'\'\n')
        f.write('exp_original_path: ' + new_collection_path + '\n')
        if add_all_paths:
            f.write('exp_ref: ' + new_collection_path + '\n')
            f.write('exp: ' + new_collection_path + '\n')
