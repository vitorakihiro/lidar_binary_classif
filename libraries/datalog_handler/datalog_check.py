#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 23:44:54 2020

@author: dell
"""
from IPython import get_ipython
get_ipython().magic('reset -sf')

import os, sys, time, yaml
import bisect
from termcolor import colored
import import_data_class 
import file_handling_utils
import cam_utils 
import numpy as np
import matplotlib.pyplot as plt

def f3(val):
    if float(val).is_integer():
        return str(int(val))
    return format(val,'3.3f')
def getFixedWidth(ss, width):
  return '{0: <{width}}'.format(ss, width=width)
def printGreen(txt, end='\n'):
    print('\x1b[1;32;47m' + txt + '\x1b[0m', end=end)
def printRed(txt, end='\n'):
    print('\x1b[1;31;47m' + txt + '\x1b[0m', end=end)
def printRedOnYellow(txt, end='\n'):
    print('\x1b[1;31;43m' + txt + '\x1b[0m', end=end)
def vec2print(vec, idx0, idx1):
  if idx0 < 0: idx0 = 0
  if idx1 >= len(vec): idx1 = len(vec) - 1
  ss = '['
  for i in range(idx0, idx1):
    ss += f3(vec[i])
    if i < idx1-1:
      ss +=', '
    else:
      ss += ']'
  return ss 

class DatalogCheck:
    def __init__(self, log, exp, check_timers=False, check_front_camera=False, check_vars=False, expected_ts_diff=25, attention_color='red'):
        self.attention_color = attention_color
        self.check_front_camera=check_front_camera
        self.check_timers=check_timers
        self.check_vars=check_vars
        self.log = log
        self.exp = exp
        self.expected_ts_diff = expected_ts_diff
        self.auto_init_time=[]
        self.high_diff_ts_idx_sys=[]
        self.high_diff_ts_idx_lidar=[]
        self.high_diff_ts_idx_pl=[]
        
    def check(self):
        print(self.exp.rjust(60,'#'))
        print('\t========================================================')
        self.checkFrontCamera()
        print('\t========================================================')
        self.checkDatalog()
        print('\t--------------------------------------------------------')
        self.checkLoggedVars(self.log.datalog, self.exp)
        print('\t========================================================')
        self.checkLidar()
        print('\t========================================================')
        self.checkPL()
        print('\t--------------------------------------------------------')
        self.checkLoggedVars(self.log.pTS, self.exp)
        print('\t========================================================')
        self.checkNav()
        print('\t--------------------------------------------------------')
        self.checkLoggedVars(self.log.nav, self.exp)

    def checkAuto(self, auto_init_idx, auto_end_idx, timestamp):
        auto_init_time = np.array([timestamp[i] for i in auto_init_idx])
        auto_end_time = np.array([timestamp[i] for i in auto_end_idx])
        auto_time = auto_end_time - auto_init_time

        print('\t\tTime in auto: ' + f3(np.sum(auto_time)) + ' s --- auto runs: ' + str(len(self.auto_init_time)) + ' auto idxs:')
        for i in range(0, len(auto_init_time)):
            print('\t\t\t[' + str(i) + ']\t(' + str(auto_init_idx[i]) + ', ' + str(auto_end_idx[i]) + ') \tduration: ' + f3(auto_time[i]) + ' s')
        return auto_init_time, auto_end_time, auto_time
    def checkDatalog(self):
        if self.exp in list(self.log.datalog) and '     (1)uptime (ms)' in list(self.log.datalog[self.exp]): 
            datalog_ts = np.array(self.log.datalog[self.exp]['     (1)uptime (ms)'])
            printRedOnYellow('\tdatalog is available.')
            print('\tDuration of ' +  f3(0.001*(datalog_ts[-1]-datalog_ts[0])) + 's. Timestamp range ' + str(datalog_ts[0]) + 'ms to ' + str(datalog_ts[-1]) + 'ms')
            self.auto_init_time, self.auto_end_time, auto_time, auto_init_idx, auto_end_idx = self.countAuto()
            not_recorded_total_time_sys, self.high_diff_ts_idx_sys = self.checkTimestampGaps(datalog_ts, 200)
        else:
            print('\t' + colored('system datalog not available', self.attention_color))
    def checkFrontCamera(self):
        if self.check_front_camera and self.exp in list(self.log.cam_front):
            print('\t', end='')
            ts_cam = cam_utils.TScam(self.log, self.exp)
            ts_cam.ref_ts = ts_cam.timestamp
            r=1
            c=5
            f = plt.figure(figsize=(40,10))
            for i in range(0, 5):
                idx = int(i*len(ts_cam.timestamp)/5)
                ax = f.add_subplot(r,c,i+1)      
                frame, diff = ts_cam.getFrame(idx, False)
                ax.imshow(frame)
                ax.set_title('timestamp ' + str(ts_cam.timestamp[int(i*len(ts_cam.timestamp)/5)]))
            plt.show()
        elif not self.check_front_camera:
            print('\tNot checking front camera')
        elif self.exp not in list(self.log.cam_front):
            print('\tfront camera data not available in exp')    
    def checkInsideAutoTime(self, ts, idx):        
        inside_auto_mode=False
        inside_auto_mode_idx=-1
        for j in range(0, len(self.auto_init_time)):
            t = ts[idx]
            if t > self.auto_init_time[j] and t < self.auto_end_time[j]:
                inside_auto_mode=True
                inside_auto_mode_idx=j
        return inside_auto_mode, inside_auto_mode_idx
    def checkLidar(self):
        if self.exp in list(self.log.lidar) and len(self.log.lidar[self.exp])>0: 
            lidar_ts = np.array(list(self.log.lidar[self.exp]))
            printRedOnYellow('\tlidar is available.')
            print('\tDuration of ' + f3(0.001*(lidar_ts[-1]-lidar_ts[0])) + 's. Timestamp range ' + str(lidar_ts[0]) + 'ms to ' + str(lidar_ts[-1]) + 'ms')
            not_recorded_total_time_lidar, self.high_diff_ts_idx_lidar = self.checkTimestampGaps(lidar_ts, self.expected_ts_diff)
        else:
            print(colored('\tlidar not available', self.attention_color))
    def checkLoggedVars(self, dfs, exp):
        if self.check_vars:
            if exp in list(dfs):
                df = dfs[exp]
                names = list(df)
                for i in range(0, len(names)):
                    all_nonzero = np.count_nonzero(df[names[i]]) > 0
                    try:
                        nan_percentage = np.count_nonzero(np.isnan(df[names[i]]))/len(df[names[i]])
                        if nan_percentage > 0:
                            print(colored('\t\t[NAN ' + f3(100*nan_percentage) + '%]', self.attention_color), end='\t')
                            print(names[i])  
                        elif not all_nonzero:
                            print(colored('\t\t[EMPTY]', self.attention_color), end='\t')  
                            print(names[i]) 
                    except:
                        print('\t\tCould not check ' + names[i])
        else:
            print('\t\tNot checking vars inside log')
    def checkNav(self):
        if self.exp in list(self.log.nav) and 'up_time' in self.log.nav[self.exp]: 
            nav_ts = np.array(list(self.log.nav[self.exp].up_time))
            printRedOnYellow('\tnav is available.')
            print('\tDuration of ' + f3(0.001*(nav_ts[-1]-nav_ts[0])) + 's. Timestamp range ' + str(nav_ts[0]) + 'ms to ' + str(nav_ts[-1]) + 'ms')
            not_recorded_total_time_nav, high_diff_ts_idx_nav = self.checkTimestampGapsSimple(nav_ts, 2*self.expected_ts_diff)
        else:
            print('\t', colored('nav not available', self.attention_color))

    def checkPL(self):
        if self.exp in list(self.log.pTS) and 'timestamp' in self.log.pTS[self.exp]: 
            p_ts = np.array(self.log.pTS[self.exp].timestamp)      
            printRedOnYellow('\tperception is available.')
            print('\tDuration of ' + f3(0.001*(p_ts[-1]-p_ts[0])) + 's. Timestamp range ' + str(p_ts[0]) + 'ms to ' + str(p_ts[-1]) + 'ms')
            if len(self.auto_init_time) == 0 and 'Automatic' in list(self.log.pTS[self.exp]):       
                print('\t\tgetting auto time from PL')         
                self.auto_init_time, self.auto_end_time, auto_time, auto_init_idx, auto_end_idx = self.countAutoFromPl()
            not_recorded_total_time_pl, self.high_diff_ts_idx_pl = self.checkTimestampGapsSimple(p_ts, self.expected_ts_diff)
            for i in range(0, len(self.high_diff_ts_idx_pl)):
                idx = self.high_diff_ts_idx_pl[i]
                diff = p_ts[idx] - p_ts[idx-1]
                sub_vec2print = vec2print(p_ts, idx-1, idx+2)
                text = getFixedWidth('\t\t\t' +str(i) + '\t(' + str(self.high_diff_ts_idx_pl[i]) + ') t[' + str(idx-1) + ':' + str(idx+2) + ']: ' + sub_vec2print + ' ms and diff of ' + str(diff) + 'ms', 80)
                if diff < 10*self.expected_ts_diff:
                    print(text, end='')
                else:
                    print(colored(text, self.attention_color), end='')
                inside_auto_mode, inside_auto_mode_idx = self.checkInsideAutoTime(0.001*p_ts, idx)
                if inside_auto_mode: 
                    print(colored('\tHappened in AUTO mode. See auto idx ' + str(inside_auto_mode_idx) , self.attention_color))
                else: print('\tMANUAL MODE')
            if self.check_timers:
                print('\t\t################### DETAILS')
                p = self.log.pTS[self.exp]
                
                for i in range(0, len(self.high_diff_ts_idx_pl)):
                    idx = self.high_diff_ts_idx_pl[i]
                    diff = p_ts[idx] - p_ts[idx-1]
                    sub_vec2print = vec2print(p_ts, idx-1, idx+2)
                    print('\t\t________________________________________________________________________________')
                    text = '\t\t' +str(i) + '\t(' + str(self.high_diff_ts_idx_pl[i]) + ') t[' + str(idx-1) + ':' + str(idx+2) + ']: ' + sub_vec2print + ' ms and diff of ' + str(diff) + 'ms'
                    if diff < 10*self.expected_ts_diff:
                        print(text, end='')
                    else:
                        print(colored(text, self.attention_color), end='')
                    inside_auto_mode, inside_auto_mode_idx = self.checkInsideAutoTime(0.001*p_ts, idx)
                    if inside_auto_mode: 
                        print(colored('\tHappened in AUTO mode. See auto idx ' + str(inside_auto_mode_idx), self.attention_color ))
                    else: 
                        print('\tMANUAL MODE')
                    self.checkTimers('t_process_lidar_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')
                    self.checkTimers('t_estimate_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')
                    self.checkTimers('t_calculate_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t')
                    self.checkTimers('t_left_side_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_right_side_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_choosedReadings_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_ekf_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedFromStart', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterSplit', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterHist', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterUpdateHist', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterLineEst', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterCheckEstimatedLaneWidth', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedAfterGetLinePoints', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedTimeBetweenMeas', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedTimeAfterHeuristics', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedTimeAfterPlEkf', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_calc_ellapsedTimeAfterUpdate', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t\t')
                    self.checkTimers('t_estimateLaneHeading_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')
                    self.checkTimers('t_estimateRemainingLane_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')
                    self.checkTimers('t_getBisect_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')
                    self.checkTimers('t_setXYlidar_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_smoothCurve_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateEncoder_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_calculatePID_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_stopRobotOutRow_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateDesiredThrottleYaw_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_setDriveCommands_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_runOdometry_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_firstIteration_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_copy_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t')                
                    self.checkTimers('t_updateLidarReadings_mutex_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t')                
                    self.checkTimers('t_updateLidarReadings_setLidarData_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t\t')                
                    self.checkTimers('t_updateHasExitedRow_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_switchPIDcontroller_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_smoothCurve_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateEncoder_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_calculatePID_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_stopRobotOutRow_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateDesiredThrottleYaw_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_setDriveCommands_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_runOdometry_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_firstIteration_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_copy_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_mutex_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateLidarReadings_setLidarData_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_updateHasExitedRow_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('t_switchPIDcontroller_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                
                    self.checkTimers('logtime_us', idx-1, idx+2, diff, multiplier=0.001, prepend_txt='\t\t')                              
        else:
            print('\t', colored('perception not available', self.attention_color))
 
    def checkTimers(self, name, idx0, idx1, compare_to_value, multiplier=1, prepend_txt=''):        
        if name in list(self.log.pTS[self.exp]):
            vec = multiplier*np.array(self.log.pTS[self.exp][name])
            if idx0 < 0: idx0 = 0
            if idx1 >= len(vec): idx1 = len(vec)-1
            percentages=[]
            for i in range(idx0, idx1):        
                percentages.append(vec[i]/compare_to_value*100)
            max_percentage = np.nanmax(percentages)
            sub_vec2print = vec2print(vec, idx0, idx1)
            print(str(prepend_txt),end='')
            if(max_percentage>20):
                print(colored(getFixedWidth('['+f3(max_percentage) + '%] ', 15), self.attention_color), end='')    
            else:
                print(getFixedWidth('['+f3(max_percentage) + '%] ', 15), end='')  
            print(getFixedWidth(sub_vec2print + ' ms', 30) + str(name))
        else:
            print(str(prepend_txt) + name + ' not in DataFrame')
    def checkTimestampGapsSimple(self, ts, expected_ts_diff):
        ts = np.array(ts)
        total_time = ts[-1] - ts[0]
        diff_ts = ts[1:]-ts[0:-1]
        diff_ts = np.insert(diff_ts, 0, 0)
        high_diff_ts_idx = []
        not_recorded_total_time = 0
        for i in range(1, len(ts)):
            if ts[i] - ts[i-1] > 5*expected_ts_diff:
                high_diff_ts_idx.append(i)
                not_recorded_total_time += ts[i] - ts[i-1]
        print('\t\tNo of log time diff > ' + str(2*expected_ts_diff) + 'ms: ' + str(len(high_diff_ts_idx)) + '.', end=' ')
        if not_recorded_total_time > 0:
            print('Total of not recorded time: ' + f3(0.001*not_recorded_total_time) + \
                ' s (' +f3(100*not_recorded_total_time/total_time) + '% of total time).\n\t\tSee idxs of those greater than ' + str(5*expected_ts_diff) + ':')
        else:
            print('')
        return not_recorded_total_time, high_diff_ts_idx

    def checkTimestampGaps(self, ts, expected_ts_diff):
        ts = np.array(ts)  
        not_recorded_total_time, high_diff_ts_idx = self.checkTimestampGapsSimple(ts,expected_ts_diff)
        for i in range(0, len(high_diff_ts_idx)):
            idx = high_diff_ts_idx[i]
            diff = ts[idx] - ts[idx-1]
            sub_vec2print = vec2print(ts, idx-1, idx+2)
            text = '\t\t\t' +str(i) + '\t(' + str(high_diff_ts_idx[i]) + ') t[' + str(idx-1) + ':' + str(idx+2) + ']: ' + sub_vec2print + ' ms and diff of ' + str(diff) + 'ms'
            if diff < 10*expected_ts_diff:
                print(text)
            else:
                print(colored(text, self.attention_color))
        return not_recorded_total_time, high_diff_ts_idx

    def countAuto(self):
        drive_mode_vec = self.log.datalog[self.exp][ '      (2)drive mode']
        datalog_ts = 0.001*np.array(self.log.datalog[self.exp]['     (1)uptime (ms)'])
        b = np.where(drive_mode_vec==' AUTO')
        auto_init_idx=[]
        auto_end_idx=[]
        auto_init_time=[]
        auto_end_time=[]
        auto_time=0
        if len(b)>0 and len(b[0]>0):
            idx = b[0]
            auto_init_idx=[idx[0]]
            auto_end_idx=[]
            prev_idx=idx[0]
            for i in range(1, len(idx)):
                if idx[i]-prev_idx>1:
                    auto_end_idx.append(idx[i-1])
                    auto_init_idx.append(idx[i])
                prev_idx=idx[i]
            if len(auto_end_idx) < len(auto_init_idx):
                auto_end_idx.append(idx[-1])
            auto_init_time, auto_end_time, auto_time = self.checkAuto(auto_init_idx, auto_end_idx, datalog_ts)
        return auto_init_time, auto_end_time, auto_time, auto_init_idx, auto_end_idx
    def countAutoFromPl(self):
        automatic = np.array(self.log.pTS[self.exp].Automatic)
        timestamp = 0.001*np.array(self.log.pTS[self.exp].timestamp)

        auto_init_idx=np.int_([])
        auto_end_idx=np.int_([])
        #auto_end_time=[]
        if automatic[0] > 0:
            auto_init_idx = np.append(auto_init_idx, 0) 
            
        for i in range(1, len(automatic)):
            if automatic[i-1] == 0 and automatic[i] > 0:
                auto_init_idx = np.append(auto_init_idx, i) 
            if automatic[i-1] > 0 and automatic[i] == 0:
        #        auto_end_time.append(i)
                auto_end_idx = np.append(auto_end_idx, i) 
        if len(auto_end_idx) < len(auto_init_idx):
            auto_end_idx = np.append(auto_end_idx, len(automatic)-1) 

        auto_init_time, auto_end_time, auto_time = self.checkAuto(auto_init_idx, auto_end_idx, timestamp)

        return auto_init_time, auto_end_time, auto_time, auto_init_idx, auto_end_idx
