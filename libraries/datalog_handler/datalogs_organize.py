#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 07:42:32 2020

@author: dell
"""

import sys, os
import shutil
def copyAndRenameFile(file, req_folder, new_file_name):
    shutil.copy(file, req_folder)
    file_copy = os.path.join(req_folder, os.path.basename(file)) 
    new_file_path = os.path.join(req_folder, new_file_name)
    print(str(file_copy) + ' renamed to ' + new_file_path )
    os.rename(file_copy, new_file_path)
    backup_dir = os.path.join(os.path.dirname(file), 'backup_files')
    backup_path = os.path.join(backup_dir, os.path.basename(file))
    os.rename(file, backup_path)

def find_pattern(filelist, pattern_vec, find_pattern_idx=False):
    split=[0]*len(filelist)
    for i, file in enumerate(filelist):
        for idx, pattern in enumerate(pattern_vec):
            if file.find(str(pattern))>0:
                if find_pattern_idx:
                    split[i] = idx
                else:
                    split[i] = pattern
    return split
def make_folder(path_to_folder, folder_name):
    req_folder = os.path.join(path_to_folder, folder_name)
    if not os.path.exists(req_folder):
        os.makedirs(req_folder)
    else:
        print('Skipped. ' + str(req_folder) + ' already exists.')
    return req_folder


def organize(path_to_logs):

    filelist = [f for f in os.listdir(path_to_logs) if os.path.isfile(os.path.join(path_to_logs,f))]
    filelist_fullpath = [os.path.join(path_to_logs,file) for file in filelist]
    filesize = [os.path.getsize(file) for file in filelist_fullpath]

    weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    year = [year for year in range(2017,2050)]
    year_split = find_pattern(filelist, year)            
                
    month = ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    month_split = find_pattern(filelist, month, find_pattern_idx=True)
    date_split = [max([file.find(pattern)+len(pattern)+1 for pattern in month]) for file in filelist]

    new_date=['collection_']*len(filelist)
    for i, idx in enumerate(date_split):
        new_date[i]+=str(year_split[i]) + '{0:02d}'.format(month_split[i]) + filelist[i][date_split[i]:date_split[i]+11]
    
    req_folder = os.path.join(path_to_logs, 'backup_files')
    if not os.path.exists(req_folder):
        os.makedirs(req_folder)
    else:
        print('Skipped. ' + str(req_folder) + ' already exists.')

    for i, file in enumerate(filelist_fullpath):
        filename, file_extension = os.path.splitext(file)
        if file_extension == '.txt':
            req_folder = os.path.join(path_to_logs, new_date[i])
            collection_folder = req_folder
            req_lidar_folder = make_folder(collection_folder, 'lidar')
            req_nav_folder = make_folder(collection_folder, 'lidar')
            req_sys_folder = make_folder(collection_folder, 'system')
            #add to yaml file
            with open('paths.yaml', 'a') as f:
                
                f.write('\n#Added with organize_datalogs.py')
                f.write('\ngt_path: \'\'\n')
                f.write('exp_original_path: ' + collection_folder + '\n')
                f.write('exp_ref: ' + collection_folder + '\n')
                f.write('exp: ' + collection_folder + '\n')

        if file.find('measurements')>0 and file.find('secondary')<0:
            copyAndRenameFile(file, req_lidar_folder, 'lidar_log')
        if file.find('perception')>0:
            copyAndRenameFile(file, req_lidar_folder, 'perception_lidar_log')
        if file.find('extended')>0:
            copyAndRenameFile(file, req_lidar_folder, 'extended_pl_log')
        if file.find('navigation')>0:
            copyAndRenameFile(file, req_nav_folder, 'nav_log')    
        
            

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        