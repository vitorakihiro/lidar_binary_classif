# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 19:07:18 2018

@author: vahh
"""
# Remove if not using spyder. The following two lines clear variables
from IPython import get_ipython
get_ipython().magic('reset -sf')

import sys, yaml
from debug_figure import *
from import_data_class import *
import file_handling_utils

# import path from paths.yaml
with open('paths.yaml') as file:
    paths = yaml.load(file)
exp = paths['exp']

exp, data_dir = file_handling_utils.adjustPathNames(paths['exp'])
#log = ImportedData(data_dir, import_exp=exp)
#
#lidar = log.lidar[exp]
#angle = np.linspace(-45,225,1081, endpoint = False)
#l_ts = list(lidar)
#

fpath = data_dir + '/' + exp + '/lidar/lidar_log' 
df = open(fpath)
content = df.read().splitlines()
lm = []
for i in range(0,len(content)):
#for i in range(0,1):
    fields = content[i].split(",")
    _lm0 = fields[1::][:-1]
    _lm = [int(l) for l in _lm0]
    lm.append(_lm)
    
new_file_name = data_dir + '/' + exp + '/lidar/new_lidar_log' 
f = open(new_file_name, 'w')
t=0
for i in range(len(lm)):
    new_line = str(t)
    l = lm[i]
    for j in range(len(l)):
        new_line += ',' + str(int(l[j]))
    new_line += '\n'
    t+=25
    f.write(new_line)
    
    