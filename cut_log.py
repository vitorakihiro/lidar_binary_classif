#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 23:44:54 2020

@author: dell
"""
from IPython import get_ipython
get_ipython().magic('clear')
get_ipython().magic('reset -sf')

import os, pathlib, sys, time, yaml
libraries = [os.path.abspath('libraries/datalog_handler')]
for lib in libraries:
    if lib not in sys.path:
        sys.path.append(lib)

import datalog_cut


# import path from paths.yaml
with open('paths.yaml') as file:
    paths = yaml.load(file)
    
print('\n\nUse datalog_cut.cut(paths[\'exp_original_path\'], t0, tf, append_name) where\n' + \
      't0 is the initial lidar timestamp (in ms)\n'
      'tf is the final lidar timestamp (in ms)\n'
      'append_name is the string that will be appended to the end of the new collection path.\n'+\
      '1. The new collection will be available in the same folder as the original one;\n' + \
      '2. The timestamps can be approximate. The script will get the nearest ones;\n' + \
      '3. Only lidar_log and perception_lidar_log will be cut;\n' + \
      '4. The path to the new collection will be automatically added to paths.yaml\n\n')
    
    
t0 = 42000
tf = 57630
append_name = 'auto'
datalog_cut.cut(paths['exp_original_path'], t0, tf, append_name)